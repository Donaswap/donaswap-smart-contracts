import { ethers, network } from "hardhat";
import { parseEther } from "ethers/lib/utils";
import config from "../../config";

const currentNetwork = network.name;

const main = async () => {
  console.log("Deploying to network:", currentNetwork);

  const _tokenPrice = parseEther("4");
  const _ipfsHash = "";
  const _startBlockTime = "";
  const _endBlockTime = "";

  const FlammyFactoryV2 = await ethers.getContractFactory("FlammyFactoryV2");

  const flammyFactory = await FlammyFactoryV2.deploy(
    config.DonaswapFlammies[currentNetwork],
    config.FlameToken[currentNetwork],
    _tokenPrice,
    _ipfsHash,
    _startBlockTime,
    _endBlockTime
  );

  await flammyFactory.deployed();
  console.log("FlammyFactoryV2 deployed to:", flammyFactory.address);

  await flammyFactory.setFlammyNames("Sleepy", "Dollop", "Twinkle", "Churro", "Sunny");
  await flammyFactory.setFlammyJson("sleepy.json", "dollop.json", "twinkle.json", "churro.json", "sunny.json");
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
