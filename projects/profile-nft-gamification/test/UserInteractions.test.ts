import { assert } from "chai";
import { expectEvent, expectRevert } from "@openzeppelin/test-helpers";
import { artifacts, contract } from "hardhat";

const { gasToBNB, gasToUSD } = require("./helpers/GasCalculation");

const MockBEP20 = artifacts.require("./utils/MockBEP20.sol");
const MockFlammies = artifacts.require("./utils/MockFlammies.sol");
const MockCats = artifacts.require("./utils/MockCats.sol");
const DonaswapProfile = artifacts.require("./DonaswapProfile.sol");

contract("User interactions", ([alice, bob, carol, david, erin, frank]) => {
  const _totalInitSupply = "50000000000000000000"; // 50 FLAME
  const _numberFlameToReactivate = "5000000000000000000";
  const _numberFlameToRegister = "5000000000000000000"; // 5 FLAME
  const _numberFlameToUpdate = "2000000000000000000"; // 2 FLAME

  let mockFlammies, mockCats, mockFlame, donaswapProfile;
  let DEFAULT_ADMIN_ROLE, SPECIAL_ROLE, NFT_ROLE, POINT_ROLE;
  let result;

  before(async () => {
    mockFlame = await MockBEP20.new("Mock FLAME", "FLAME", _totalInitSupply, {
      from: alice,
    });

    mockFlammies = await MockFlammies.new({ from: alice });

    donaswapProfile = await DonaswapProfile.new(
      mockFlame.address,
      _numberFlameToReactivate,
      _numberFlameToRegister,
      _numberFlameToUpdate,
      { from: alice }
    );

    DEFAULT_ADMIN_ROLE = await donaswapProfile.DEFAULT_ADMIN_ROLE();
    NFT_ROLE = await donaswapProfile.NFT_ROLE();
    POINT_ROLE = await donaswapProfile.POINT_ROLE();
    SPECIAL_ROLE = await donaswapProfile.SPECIAL_ROLE();
  });

  // Check ticker, symbols, supply, and owners are correct
  describe("All contracts are deployed correctly", async () => {
    it("Initial parameters are correct for MockFlammies", async () => {
      assert.equal(await mockFlammies.name(), "Mock Flammies");
      assert.equal(await mockFlammies.symbol(), "MB");
      assert.equal(await mockFlammies.balanceOf(alice), "0");
      assert.equal(await mockFlammies.totalSupply(), "0");
      assert.equal(await mockFlammies.owner(), alice);
    });

    it("Initial parameters are correct for MockFLAME", async () => {
      assert.equal(await mockFlame.name(), "Mock FLAME");
      assert.equal(await mockFlame.symbol(), "FLAME");
      assert.equal(await mockFlame.balanceOf(alice), "50000000000000000000");
      assert.equal(await mockFlame.totalSupply(), "50000000000000000000");
    });

    it("Initial parameters are correct for DonaswapProfile", async () => {
      assert.equal(await donaswapProfile.flameToken(), mockFlame.address);
      assert.equal(await donaswapProfile.numberFlameToRegister(), _numberFlameToRegister);
      assert.equal(await donaswapProfile.numberFlameToUpdate(), _numberFlameToUpdate);
      assert.equal(await donaswapProfile.numberFlameToReactivate(), _numberFlameToReactivate);

      for (let role of [SPECIAL_ROLE, NFT_ROLE, POINT_ROLE]) {
        assert.equal(await donaswapProfile.getRoleMemberCount(role), "0");
      }

      assert.equal(await donaswapProfile.getRoleMemberCount(DEFAULT_ADMIN_ROLE), "1");
    });
  });

  describe("Logic to register and create team works as expected", async () => {
    it("Bob cannot create a profile if teamId is invalid", async () => {
      await expectRevert(
        donaswapProfile.createProfile("0", mockFlammies.address, "0", {
          from: bob,
        }),
        "Invalid teamId"
      );
    });

    it("Alice creates a team and data is reflected accordingly", async () => {
      result = await donaswapProfile.addTeam("The Testers", "ipfs://hash/team1.json", {
        from: alice,
      });

      expectEvent(result, "TeamAdd", {
        teamId: "1",
        teamName: "The Testers",
      });

      // Verify the team is created properly
      result = await donaswapProfile.getTeamProfile("1");
      assert.equal(result[0], "The Testers");
      assert.equal(result[1], "ipfs://hash/team1.json");
      assert.equal(result[2], "0");
      assert.equal(result[3], "0");
      assert.equal(result[4], true);
    });

    it("Bob cannot mint a NFT if contract address not supported", async () => {
      await expectRevert(
        donaswapProfile.createProfile("1", mockFlammies.address, "0", {
          from: bob,
        }),
        "NFT address invalid"
      );

      result = await donaswapProfile.addNftAddress(mockFlammies.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: NFT_ROLE,
        account: mockFlammies.address,
        sender: alice,
      });

      assert.equal(await donaswapProfile.getRoleMemberCount(NFT_ROLE), "1");
    });

    it("Bob cannot create a profile without a NFT to spend", async () => {
      // Bob is trying to spend a token that doesn't exist
      await expectRevert(
        donaswapProfile.createProfile("1", mockFlammies.address, "0", {
          from: bob,
        }),
        "ERC721: owner query for nonexistent token"
      );

      // Bob mints a NFT
      await mockFlammies.mint({ from: bob });
      assert.equal(await mockFlammies.balanceOf(bob), "1");
      assert.equal(await mockFlammies.ownerOf("0"), bob);
      assert.equal(await mockFlammies.totalSupply(), "1");
    });

    it("Bob cannot create a profile without approving the NFT to be spent", async () => {
      // Bob will not be able to transfer because contract not approved
      await expectRevert(
        donaswapProfile.createProfile("1", mockFlammies.address, "0", {
          from: bob,
        }),
        "ERC721: transfer caller is not owner nor approved"
      );

      // Bob approves the contract to receive his NFT
      result = await mockFlammies.approve(donaswapProfile.address, "0", {
        from: bob,
      });

      expectEvent(result, "Approval", {
        owner: bob,
        approved: donaswapProfile.address,
        tokenId: "0",
      });
    });

    it("Bob cannot create a profile without FLAME tokens in his wallet", async () => {
      // Bob doesn't have FLAME
      await expectRevert(
        donaswapProfile.createProfile("1", mockFlammies.address, "0", {
          from: bob,
        }),
        "BEP20: transfer amount exceeds balance"
      );

      // Bob mints 10 FLAME
      for (let i = 0; i < 5; i++) {
        await mockFlame.mintTokens("2000000000000000000", { from: bob });
      }

      // Bob has the proper balance
      assert.equal(await mockFlame.balanceOf(bob), "10000000000000000000");
    });

    it("Bob cannot create a profile without FLAME token approval to be spent", async () => {
      // Bob didn't approve FLAME to be spent
      await expectRevert(
        donaswapProfile.createProfile("1", mockFlammies.address, "0", {
          from: bob,
        }),
        "BEP20: transfer amount exceeds allowance"
      );

      // Bob approves the FLAME token to be spent
      result = await mockFlame.approve(donaswapProfile.address, "5000000000000000000", { from: bob });

      expectEvent(result, "Approval", {
        owner: bob,
        spender: donaswapProfile.address,
        value: "5000000000000000000", // 5 FLAME
      });
    });

    it("Bob can finally create a profile and data is reflected as expected", async () => {
      result = await donaswapProfile.createProfile("1", mockFlammies.address, "0", {
        from: bob,
      });

      expectEvent(result, "UserNew", {
        userAddress: bob,
        teamId: "1",
        nftAddress: mockFlammies.address,
        tokenId: "0",
      });

      // Team 1 has 1 user
      result = await donaswapProfile.getTeamProfile("1");
      assert.equal(result[2], "1");

      // Verify Bob's balance went down and allowance must be 0
      assert.equal(await mockFlame.balanceOf(bob), "5000000000000000000");
      assert.equal(await mockFlame.allowance(bob, donaswapProfile.address), "0");

      // Verify that the mock NFT went to the contract
      assert.equal(await mockFlammies.balanceOf(bob), "0");
      assert.equal(await mockFlammies.ownerOf("0"), donaswapProfile.address);

      // Verify number of active profiles changed
      assert.equal(await donaswapProfile.numberActiveProfiles(), "1");

      // Verify Bob has registered
      assert.equal(await donaswapProfile.hasRegistered(bob), true);
    });

    it("Bob cannot register twice", async () => {
      await expectRevert(
        donaswapProfile.createProfile("1", mockFlammies.address, "0", {
          from: bob,
        }),
        "Already registered"
      );
    });
  });

  describe("Logic to pause and reactivate a profile works as expected", async () => {
    it("Bob only can pause his profile", async () => {
      result = await donaswapProfile.pauseProfile({
        from: bob,
      });

      expectEvent(result, "UserPause", {
        userAddress: bob,
        teamId: "1",
      });

      result = await donaswapProfile.getUserStatus(bob);
      assert.equal(result, false);
    });

    it("NFT returned to Bob and contract statuses were updated", async () => {
      // Verify that the mock NFT went back to Bob
      assert.equal(await mockFlammies.balanceOf(bob), "1");
      assert.equal(await mockFlammies.ownerOf("0"), bob);

      // Verify there is no more active user
      assert.equal(await donaswapProfile.numberActiveProfiles(), "0");

      // Verify the team has 0 active user
      result = await donaswapProfile.getTeamProfile("1");
      assert.equal(result[2], "0");
    });

    it("Bob cannot pause again/update while paused/register", async () => {
      // Bob cannot pause a profile twice
      await expectRevert(
        donaswapProfile.pauseProfile({
          from: bob,
        }),
        "User not active"
      );

      // Bob cannot update his own profile after it is paused
      await expectRevert(
        donaswapProfile.updateProfile(mockFlammies.address, "0", {
          from: bob,
        }),
        "User not active"
      );

      // Bob cannot re-register
      await expectRevert(
        donaswapProfile.pauseProfile({
          from: bob,
        }),
        "User not active"
      );
    });

    it("Bob reactivates his profile", async () => {
      // Bob increases allowance for address
      result = await mockFlame.increaseAllowance(donaswapProfile.address, "5000000000000000000", { from: bob });

      expectEvent(result, "Approval", {
        owner: bob,
        spender: donaswapProfile.address,
        value: "5000000000000000000", // 5 FLAME
      });

      // Bob approves the contract to receive his NFT
      result = await mockFlammies.approve(donaswapProfile.address, "0", {
        from: bob,
      });

      expectEvent(result, "Approval", {
        owner: bob,
        approved: donaswapProfile.address,
        tokenId: "0",
      });

      // Bob reactivates his profile
      result = await donaswapProfile.reactivateProfile(mockFlammies.address, "0", {
        from: bob,
      });

      expectEvent(result, "UserReactivate", {
        userAddress: bob,
        teamId: "1",
        nftAddress: mockFlammies.address,
        tokenId: "0",
      });

      // Verify there is one active user again
      assert.equal(await donaswapProfile.numberActiveProfiles(), "1");
      // Verify the team has 1 active user again
      result = await donaswapProfile.getTeamProfile("1");
      assert.equal(result[2], "1");

      result = await donaswapProfile.getUserStatus(bob);
      assert.equal(result, true);
    });

    it("Bob cannot reactivate his profile if active", async () => {
      await expectRevert(
        donaswapProfile.reactivateProfile(mockFlammies.address, "0", {
          from: bob,
        }),
        "User is active"
      );
    });
  });

  describe("Multiple users join the system", async () => {
    it("Carol and David mints FLAME/NFTs", async () => {
      // Carol gets 10 FLAME and mint NFT
      for (let i = 0; i < 20; i++) {
        await mockFlame.mintTokens("2000000000000000000", { from: carol });
      }

      await mockFlammies.mint({ from: carol });

      // David gets 10 FLAME and mint NFT
      for (let i = 0; i < 5; i++) {
        await mockFlame.mintTokens("2000000000000000000", { from: david });
      }

      await mockFlammies.mint({ from: david });

      assert.equal(await mockFlammies.totalSupply(), "3");

      // Carol approves NFTs to be spent
      result = await mockFlammies.approve(donaswapProfile.address, "1", {
        from: carol,
      });

      expectEvent(result, "Approval", {
        owner: carol,
        approved: donaswapProfile.address,
        tokenId: "1",
      });

      // Carol approves the FLAME token to be spent
      result = await mockFlame.approve(
        donaswapProfile.address,
        "100000000000000000000", // 100 FLAME
        { from: carol }
      );

      expectEvent(result, "Approval", {
        owner: carol,
        spender: donaswapProfile.address,
        value: "100000000000000000000", // 100 FLAME
      });
    });

    it("Carol tries to spend the David's NFT", async () => {
      // Carol cannot spend the NFT of David WITHOUT his consent
      await expectRevert(
        donaswapProfile.createProfile("1", mockFlammies.address, "2", {
          from: carol,
        }),
        "Only NFT owner can register"
      );

      // David approves NFTs to be spent by Carol
      result = await mockFlammies.approve(carol, "2", {
        from: david,
      });

      expectEvent(result, "Approval", {
        owner: david,
        approved: carol,
        tokenId: "2",
      });

      // Carol cannot spend the NFT of David WITH his consent
      await expectRevert(
        donaswapProfile.createProfile("1", mockFlammies.address, "2", {
          from: carol,
        }),
        "Only NFT owner can register"
      );
    });

    it("Carol creates a profile with her NFT", async () => {
      result = await donaswapProfile.createProfile("1", mockFlammies.address, "1", { from: carol });

      expectEvent(result, "UserNew", {
        userAddress: carol,
        teamId: "1",
        nftAddress: mockFlammies.address,
        tokenId: "1",
      });
    });

    it("David registers and all statuses are updated accordingly", async () => {
      // David activates his profile
      await mockFlammies.approve(donaswapProfile.address, "2", {
        from: david,
      });

      // David approves the FLAME token to be spent
      await mockFlame.approve(
        donaswapProfile.address,
        "10000000000000000000", // 10 FLAME
        { from: david }
      );
      result = await donaswapProfile.createProfile("1", mockFlammies.address, "2", { from: david });

      expectEvent(result, "UserNew", {
        userAddress: david,
        teamId: "1",
        nftAddress: mockFlammies.address,
        tokenId: "2",
      });

      result = await donaswapProfile.getTeamProfile("1");
      assert.equal(result[2], "3");
      assert.equal(await donaswapProfile.numberActiveProfiles(), "3");
    });
  });

  describe("Multiple NFT contracts are supported and it is possible to update profile", async () => {
    it("Alice deploys and approves a new NFT contract", async () => {
      // Alice deploys new NFT contract
      mockCats = await MockCats.new({ from: alice });

      // Erin mints first tokenId (0) for MockCats
      await mockCats.mint({ from: erin });

      // Carol mints second tokenId (1) for MockCats
      await mockCats.mint({ from: carol });
    });

    it("Carol cannot update her profile until it is approved", async () => {
      await expectRevert(
        donaswapProfile.updateProfile(mockCats.address, "1", {
          from: carol,
        }),
        "NFT address invalid"
      );
    });

    it("Carol pauses her profile and tries to reactivate with new NFT", async () => {
      result = await donaswapProfile.pauseProfile({
        from: carol,
      });

      expectEvent(result, "UserPause", {
        userAddress: carol,
        teamId: "1",
      });

      // Carol approves NFT to be spent by
      await mockFlammies.approve(donaswapProfile.address, "1", {
        from: carol,
      });

      await expectRevert(
        donaswapProfile.reactivateProfile(mockCats.address, "1", {
          from: carol,
        }),
        "NFT address invalid"
      );

      result = await donaswapProfile.reactivateProfile(mockFlammies.address, "1", {
        from: carol,
      });

      expectEvent(result, "UserReactivate", {
        userAddress: carol,
        teamId: "1",
        nftAddress: mockFlammies.address,
        tokenId: "1",
      });
    });

    it("Alice approves a new NFT contract", async () => {
      // Alice adds the new NFT contract as supported
      result = await donaswapProfile.addNftAddress(mockCats.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: NFT_ROLE,
        account: mockCats.address,
        sender: alice,
      });
    });

    it("Carol pauses her profile and tries to reactivate with Erin's NFT", async () => {
      result = await donaswapProfile.pauseProfile({
        from: carol,
      });

      expectEvent(result, "UserPause", {
        userAddress: carol,
        teamId: "1",
      });

      // Erin approves her NFT contract to be spent by Carol
      await mockCats.approve(carol, "0", {
        from: erin,
      });

      await expectRevert(
        donaswapProfile.reactivateProfile(mockCats.address, "0", {
          from: carol,
        }),
        "Only NFT owner can update"
      );

      // Carol approves NFT to be spent by
      await mockCats.approve(donaswapProfile.address, "1", {
        from: carol,
      });

      result = await donaswapProfile.reactivateProfile(mockCats.address, "1", {
        from: carol,
      });

      expectEvent(result, "UserReactivate", {
        userAddress: carol,
        teamId: "1",
        nftAddress: mockCats.address,
        tokenId: "1",
      });
    });

    it("Erin let Carol spends her NFT for the profile but it reverts", async () => {
      // Erin approves her NFT contract to be spent by Carol
      await mockCats.approve(carol, "0", {
        from: erin,
      });

      await expectRevert(
        donaswapProfile.updateProfile(mockCats.address, "0", {
          from: carol,
        }),
        "Only NFT owner can update"
      );
    });

    it("Erin mints and registers her token", async () => {
      // Erin mints 10 FLAME
      for (let i = 0; i < 5; i++) {
        await mockFlame.mintTokens("2000000000000000000", { from: erin });
      }

      // Erin approves her NFT contract to be spent by DonaswapProfile
      await mockCats.approve(donaswapProfile.address, "0", {
        from: erin,
      });

      // Erin approves the FLAME token to be spent by DonaswapProfile
      await mockFlame.approve(
        donaswapProfile.address,
        "10000000000000000000", // 10 FLAME
        { from: erin }
      );

      // Erin creates her Donaswap profile
      result = await donaswapProfile.createProfile("1", mockCats.address, "0", {
        from: erin,
      });

      expectEvent(result, "UserNew", {
        userAddress: erin,
        teamId: "1",
        nftAddress: mockCats.address,
        tokenId: "0",
      });

      assert.equal(await donaswapProfile.numberActiveProfiles(), "4");
      assert.equal(await donaswapProfile.numberTeams(), "1");
    });

    it("Frank cannot call functions without a profiles", async () => {
      await expectRevert(
        donaswapProfile.pauseProfile({
          from: frank,
        }),
        "Has not registered"
      );

      await expectRevert(
        donaswapProfile.updateProfile(mockCats.address, "0", {
          from: frank,
        }),
        "Has not registered"
      );

      await expectRevert(
        donaswapProfile.reactivateProfile(mockCats.address, "0", {
          from: frank,
        }),
        "Has not registered"
      );
    });

    it("Erin updates her profile and changes the NFT contract she had", async () => {
      // Erin mints a token for MockFlammies
      await mockFlammies.mint({ from: erin });

      result = await donaswapProfile.getUserProfile(erin);
      assert.equal(result[0], "4");
      assert.equal(result[1], "0");
      assert.equal(result[2], "1");
      assert.equal(result[3], mockCats.address);
      assert.equal(result[4], "0");
      assert.equal(result[5], true);

      // Verify the number of users in team 1 is 4
      result = await donaswapProfile.getTeamProfile("1");
      assert.equal(result[2], "4");

      // Erin approves her NFT contract to be spent by DonaswapProfile
      await mockFlammies.approve(donaswapProfile.address, "3", {
        from: erin,
      });

      result = await donaswapProfile.updateProfile(mockFlammies.address, "3", {
        from: erin,
      });

      expectEvent(result, "UserUpdate", {
        userAddress: erin,
        nftAddress: mockFlammies.address,
        tokenId: "3",
      });

      // Balance checks
      assert.equal(await mockFlame.balanceOf(erin), "3000000000000000000");
      assert.equal(await mockFlammies.balanceOf(erin), "0");
      assert.equal(await mockFlammies.ownerOf("3"), donaswapProfile.address);
      assert.equal(await mockFlammies.balanceOf(donaswapProfile.address), "3");
      assert.equal(await mockCats.balanceOf(erin), "1");
      assert.equal(await mockCats.balanceOf(donaswapProfile.address), "1");
      assert.equal(await mockCats.ownerOf("0"), erin);

      // Checking Erin's profile reflects the changes
      result = await donaswapProfile.getUserProfile(erin);
      assert.equal(result[3], mockFlammies.address);
      assert.equal(result[4], "3");
    });

    it("Tests for view functions", async () => {
      result = await donaswapProfile.getUserProfile(erin);
      assert.equal(result[3], mockFlammies.address);
      assert.equal(result[4], "3");

      // Frank has no profile
      await expectRevert(donaswapProfile.getUserProfile(frank), "Not registered");

      // teamId doesn't exist
      await expectRevert(donaswapProfile.getTeamProfile("5"), "teamId invalid");
    });
  });
});
