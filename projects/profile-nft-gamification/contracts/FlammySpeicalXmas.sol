// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import {IDonaswapProfile} from "./interfaces/IDonaswapProfile.sol";
import "./FlammyMintingStation.sol";

contract FlammySpecialXmas is Ownable {
    FlammyMintingStation public immutable flammyMintingStation;
    IDonaswapProfile public immutable donaswapProfile;

    uint256 public endBlock; // End of the distribution

    // Donaswap Profile points threshold
    uint256 public donaswapProfileThresholdPoints;

    uint8 public immutable nftId; // Nft can be minted
    string public tokenURI; // Nft token URI

    mapping(address => bool) public hasClaimed;

    event FlammyMint(address indexed to, uint256 indexed tokenId, uint8 indexed flammyId);
    event NewEndBlock(uint256 endBlock);
    event NewDonaswapProfileThresholdPoints(uint256 thresholdPoints);
    event NewTokenURI(string tokenURI);

    /**
     * @notice It initializes the contract.
     * @param _flammyMintingStationAddress: FlammyMintingStation address
     * @param _donaswapProfileAddress: DonaswapProfile address
     * @param _donaswapProfileThresholdPoints: User points threshold for mint NFT
     * @param _nftId: Nft can be minted
     * @param _endBlock: the end of the block
     */
    constructor(
        address _flammyMintingStationAddress,
        address _donaswapProfileAddress,
        uint256 _donaswapProfileThresholdPoints,
        uint8 _nftId,
        uint256 _endBlock
    ) public {
        flammyMintingStation = FlammyMintingStation(_flammyMintingStationAddress);
        donaswapProfile = IDonaswapProfile(_donaswapProfileAddress);
        donaswapProfileThresholdPoints = _donaswapProfileThresholdPoints;
        nftId = _nftId;
        endBlock = _endBlock;
    }

    /**
     * @notice Update end block for distribution
     * @dev Only callable by owner.
     */
    function updateEndBlock(uint256 _newEndBlock) external onlyOwner {
        endBlock = _newEndBlock;
        emit NewEndBlock(_newEndBlock);
    }

    /**
     * @notice Update thresholdPoints for distribution
     * @dev Only callable by owner.
     */
    function updateThresholdPoints(uint256 _newThresholdPoints) external onlyOwner {
        donaswapProfileThresholdPoints = _newThresholdPoints;
        emit NewDonaswapProfileThresholdPoints(_newThresholdPoints);
    }

    /**
     * @notice Update tokenURI for distribution
     * @dev Only callable by owner.
     */
    function updateTokenURI(string memory _newTokenURI) external onlyOwner {
        tokenURI = _newTokenURI;
        emit NewTokenURI(_newTokenURI);
    }

    /**
     * @notice Mint a NFT from the FlammyMintingStation contract.
     * @dev Users can claim once. It maps to the teamId.
     */
    function mintNFT() external {
        require(canClaim(msg.sender), "User: Not eligible");
        hasClaimed[msg.sender] = true;
        // Mint collectible and send it to the user.
        uint256 tokenId = flammyMintingStation.mintCollectible(msg.sender, tokenURI, nftId);
        emit FlammyMint(msg.sender, tokenId, nftId);
    }

    /**
     * @notice Check if user can claim NFT.
     */
    function canClaim(address _userAddress) public view returns (bool) {
        (, uint256 numberUserPoints, , , , bool active) = donaswapProfile.getUserProfile(_userAddress);
        // If user is able to mint this NFT
        if (
            !hasClaimed[_userAddress] &&
            block.number < endBlock &&
            active &&
            numberUserPoints >= donaswapProfileThresholdPoints
        ) {
            return true;
        }
        return false;
    }
}
