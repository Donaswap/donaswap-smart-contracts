// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";

import "donaswap-flame-vault/contracts/FlameVault.sol";

import "./FlammyMintingStation.sol";
import "./DonaswapProfile.sol";

/**
 * @title FlammySpecialFlameVault.
 * @notice It is a contract for users to mint Flame Vault collectible.
 */
contract FlammySpecialFlameVault is Ownable {
    using SafeMath for uint256;

    FlammyMintingStation public flammyMintingStation;
    FlameVault public flameVault;
    DonaswapProfile public donaswapProfile;

    uint8 public constant flammyId = 16;

    // Collectible-related.
    uint256 public endBlock;
    uint256 public thresholdTimestamp;

    // Donaswap Profile related.
    uint256 public numberPoints;
    uint256 public campaignId;

    string public tokenURI;

    // Map if address has already claimed a NFT
    mapping(address => bool) public hasClaimed;

    event FlammyMint(address indexed to, uint256 indexed tokenId, uint8 indexed flammyId);
    event NewCampaignId(uint256 campaignId);
    event NewEndBlock(uint256 endBlock);
    event NewNumberPoints(uint256 numberPoints);
    event NewThresholdTimestamp(uint256 thresholdTimestamp);

    constructor(
        address _flameVault,
        address _flammyMintingStation,
        address _donaswapProfile,
        uint256 _endBlock,
        uint256 _thresholdTimestamp,
        uint256 _numberPoints,
        uint256 _campaignId,
        string memory _tokenURI
    ) public {
        flameVault = FlameVault(_flameVault);
        flammyMintingStation = FlammyMintingStation(_flammyMintingStation);
        donaswapProfile = DonaswapProfile(_donaswapProfile);
        endBlock = _endBlock;
        thresholdTimestamp = _thresholdTimestamp;
        numberPoints = _numberPoints;
        campaignId = _campaignId;
        tokenURI = _tokenURI;
    }

    /**
     * @notice Mint a NFT from the FlammyMintingStation contract.
     * @dev Users can claim once.
     */
    function mintNFT() external {
        require(block.number < endBlock, "TOO_LATE");

        // Check msg.sender has not claimed
        require(!hasClaimed[msg.sender], "ERR_HAS_CLAIMED");

        bool isUserActive;
        (, , , , , isUserActive) = donaswapProfile.getUserProfile(msg.sender);

        require(isUserActive, "ERR_USER_NOT_ACTIVE");

        bool isUserEligible;
        isUserEligible = _canClaim(msg.sender);

        require(isUserEligible, "ERR_USER_NOT_ELIGIBLE");

        // Update that msg.sender has claimed
        hasClaimed[msg.sender] = true;

        // Mint collectible and send it to the user.
        uint256 tokenId = flammyMintingStation.mintCollectible(msg.sender, tokenURI, flammyId);

        // Increase point on Donaswap profile, for a given campaignId.
        donaswapProfile.increaseUserPoints(msg.sender, numberPoints, campaignId);

        emit FlammyMint(msg.sender, tokenId, flammyId);
    }

    /**
     * @notice Change the campaignId for Donaswap Profile.
     * @dev Only callable by owner.
     */
    function changeCampaignId(uint256 _campaignId) external onlyOwner {
        campaignId = _campaignId;

        emit NewCampaignId(_campaignId);
    }

    /**
     * @notice Change end block for distribution
     * @dev Only callable by owner.
     */
    function changeEndBlock(uint256 _endBlock) external onlyOwner {
        endBlock = _endBlock;

        emit NewEndBlock(_endBlock);
    }

    /**
     * @notice Change the number of points for Donaswap Profile.
     * @dev Only callable by owner.
     */
    function changeNumberPoints(uint256 _numberPoints) external onlyOwner {
        numberPoints = _numberPoints;

        emit NewNumberPoints(_numberPoints);
    }

    /**
     * @notice Change threshold timestamp for distribution
     * @dev Only callable by owner.
     */
    function changeThresholdTimestamp(uint256 _thresholdTimestamp) external onlyOwner {
        thresholdTimestamp = _thresholdTimestamp;

        emit NewThresholdTimestamp(_thresholdTimestamp);
    }

    /**
     * @notice Check if a user can claim.
     */
    function canClaim(address _userAddress) external view returns (bool) {
        return _canClaim(_userAddress);
    }

    /**
     * @notice Check if a user can claim.
     */
    function _canClaim(address _userAddress) internal view returns (bool) {
        if (hasClaimed[_userAddress]) {
            return false;
        } else {
            if (!donaswapProfile.getUserStatus(_userAddress)) {
                return false;
            } else {
                uint256 lastDepositedTime;
                (, lastDepositedTime, , ) = flameVault.userInfo(_userAddress);

                if (lastDepositedTime != 0) {
                    if (lastDepositedTime < thresholdTimestamp) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
    }
}
