# FlammySpecialPrediction

This document explains how the `FlammySpecialPrediction` contract works.

`FlammySpecialPrediction` inherits from [`Ownable`](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Owanable.sol). Hence, it also inherits from [`Context`](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/Context.sol).

## 1. Variables

### Public variables

#### Numeric variables (uint256)

- `endBlock`: maximum block number to claim
- `thresholdTimestamp`: maximum timestamp if to claim (based on last action)

#### Mappings

- `flammyTokenURI`: maps a`flammyId` to its characteristics (tokenURI)
- `hasClaimed`: checks whether a user has claimed for a specific `flammyId`.

### Private variables

#### Numeric variables (uint8)

- `previousNumberFlammyIds`: number of series before (i.e., 10)

## 2. Functions

Note: all functions that are not inherited are external.

### Users

- `mintNFT(_tokenId)`: mint a new NFT (with `_tokenId` from `previousNumberFlammyIds++`). It is only possible if the user has NOT claimed in this contract.

### Owner

- `addFlammy(uint8 _flammyId, string calldata _tokenURI)`: add a new flammy with its associated characteristics
- `changeEndBlock(uint256 _endBlock)`: update maximum block number to claim flammy
- `changeThresholdTimestamp(uint256 _thresholdTimestamp)`: update maximum claimable timestamp based on last action (on Flame Vault) for the address

### View

- `canClaim(address _userAddress) returns (bool)`: check whether an address can claim (in order to mint) the flammy

## 3. Events

### User-related

```
event FlammyMint(
    address indexed to,
    uint256 indexed tokenId,
    uint8 indexed flammyId
);
```

It indicates when a new flammy is minted.

### Admin-related

```
event FlammyAdd(
    uint8 indexed flammyId
);
```

It notifies that a new flammy was added for minting.

```
event NewEndBlock(
    uint256 endBlock
);

event NewThresholdTimestamp(
    uint256 thresholdTimestamp
);
```

It notifies that one of the flammies' requirements to mint has changed.