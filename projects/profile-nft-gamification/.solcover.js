// Contracts are compiled without optimization
// and with gas estimation distortion
// https://github.com/sc-forks/solidity-coverage/blob/master/HARDHAT_README.md#usage

module.exports = {
  skipFiles: [
    "IDonaswapProfile.sol",
    "test/FlameToken.sol",
    "test/MasterChef.sol",
    "test/SyrupBar.sol",
    "utils/IFO.sol",
    "utils/MockAdmin.sol",
    "utils/MockBEP20.sol",
    "utils/MockFlammies.sol",
    "utils/MockCats.sol",
    "old/FlammyFactoryV2.sol",
    "old/FlammyMintingFarm.sol",
  ],
  measureStatementCoverage: false,
  measureFunctionCoverage: true,
};
