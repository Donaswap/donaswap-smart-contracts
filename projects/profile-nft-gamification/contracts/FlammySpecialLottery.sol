// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";

import {IDonaswapLottery} from "lottery/contracts/interfaces/IDonaswapLottery.sol";
import {FlammyMintingStation} from "./FlammyMintingStation.sol";
import {DonaswapProfile} from "./DonaswapProfile.sol";

contract FlammySpecialLottery is Ownable {
    /*** Contracts ***/

    IDonaswapLottery public donaswapLottery;
    FlammyMintingStation public flammyMintingStation;
    DonaswapProfile public donaswapProfile;

    /*** Storage ***/

    uint8 constant nftId1 = 18;
    uint8 constant nftId2 = 19;
    uint8 constant nftId3 = 20;

    uint256 public endBlock; // End of the distribution
    uint256 public startLotteryRound;
    uint256 public finalLotteryRound;

    mapping(uint8 => uint256) public campaignIds;
    mapping(uint8 => uint256) public numberPoints;
    mapping(uint8 => string) public tokenURIs;
    mapping(address => bool) public userWhitelistForNft3;
    mapping(address => mapping(uint8 => bool)) public hasClaimed;

    /*** Events ***/

    event FlammyMint(address indexed to, uint256 indexed tokenId, uint8 indexed flammyId);
    event NewAddressWhitelisted(address[] users);
    event NewCampaignId(uint8 flammyId, uint256 campaignId);
    event NewEndBlock(uint256 endBlock);
    event NewLotteryRounds(uint256 startLotteryRound, uint256 finalLotteryRound);
    event NewNumberPoints(uint8 flammyId, uint256 numberPoints);
    event NewTokenURI(uint8 flammyId, string tokenURI);

    /*** Constructor ***/

    constructor(
        address _donaswapLotteryAddress,
        address _flammyMintingStationAddress,
        address _donaswapProfileAddress,
        uint256 _endBlock,
        string memory _tokenURI1,
        string memory _tokenURI2,
        string memory _tokenURI3,
        uint256 _numberPoints1,
        uint256 _numberPoints2,
        uint256 _numberPoints3,
        uint256 _campaignId1,
        uint256 _campaignId2,
        uint256 _campaignId3,
        uint256 _startLotteryRound,
        uint256 _finalLotteryRound
    ) public {
        donaswapLottery = IDonaswapLottery(_donaswapLotteryAddress);
        flammyMintingStation = FlammyMintingStation(_flammyMintingStationAddress);
        donaswapProfile = DonaswapProfile(_donaswapProfileAddress);

        endBlock = _endBlock;

        tokenURIs[nftId1] = _tokenURI1;
        tokenURIs[nftId2] = _tokenURI2;
        tokenURIs[nftId3] = _tokenURI3;

        numberPoints[nftId1] = _numberPoints1;
        numberPoints[nftId2] = _numberPoints2;
        numberPoints[nftId3] = _numberPoints3;

        campaignIds[nftId1] = _campaignId1;
        campaignIds[nftId2] = _campaignId2;
        campaignIds[nftId3] = _campaignId3;

        startLotteryRound = _startLotteryRound;
        finalLotteryRound = _finalLotteryRound;
    }

    modifier validNftId(uint8 _flammyId) {
        require(_flammyId >= nftId1 && _flammyId <= nftId3, "NFT: Id out of range");
        _;
    }

    /*** External ***/

    /**
     * @notice Mint a NFT from the FlammyMintingStation contract.
     * @dev Users can claim once. It maps to the teamId.
     * @param _lotteryId See _canClaim documentation
     * @param _cursor See _canClaim documentation
     */
    function mintNFT(
        uint8 _flammyId,
        uint256 _lotteryId,
        uint256 _cursor
    ) external validNftId(_flammyId) {
        require(_canClaim(msg.sender, _flammyId, _lotteryId, _cursor), "User: Not eligible");

        hasClaimed[msg.sender][_flammyId] = true;

        // Mint collectible and send it to the user.
        uint256 tokenId = flammyMintingStation.mintCollectible(msg.sender, tokenURIs[_flammyId], _flammyId);

        // Increase point on Donaswap profile, for a given campaignId.
        donaswapProfile.increaseUserPoints(msg.sender, numberPoints[_flammyId], campaignIds[_flammyId]);

        emit FlammyMint(msg.sender, tokenId, _flammyId);
    }

    /**
     * @notice Check if a user can claim NFT1
     * @dev External function are cheaper than public. Helpers for external calls only.
     * @param _lotteryId See _canClaim documentation
     */
    function canClaimNft1(address _userAddress, uint256 _lotteryId) external view returns (bool) {
        return _canClaim(_userAddress, nftId1, _lotteryId, 0);
    }

    /**
     * @notice Check if a user can claim NFT2
     * @dev External function are cheaper than public. Helpers for external calls only.
     * @param _lotteryId See _canClaim documentation
     * @param _cursor See _canClaim documentation
     */
    function canClaimNft2(
        address _userAddress,
        uint256 _lotteryId,
        uint256 _cursor
    ) external view returns (bool) {
        return _canClaim(_userAddress, nftId2, _lotteryId, _cursor);
    }

    /**
     * @notice Check if a user can claim NFT3
     * @dev External function are cheaper than public. Helpers for external calls only.
     */
    function canClaimNft3(address _userAddress) external view returns (bool) {
        return _canClaim(_userAddress, nftId3, startLotteryRound, 0);
    }

    /*** External - Owner ***/

    /**
     * @notice Change end block for distribution
     * @dev Only callable by owner.
     */
    function changeEndBlock(uint256 _endBlock) external onlyOwner {
        endBlock = _endBlock;
        emit NewEndBlock(_endBlock);
    }

    /**
     * @notice Change the campaignId for Donaswap Profile.
     * @dev Only callable by owner.
     */
    function changeCampaignId(uint8 _flammyId, uint256 _campaignId) external onlyOwner validNftId(_flammyId) {
        campaignIds[_flammyId] = _campaignId;
        emit NewCampaignId(_flammyId, _campaignId);
    }

    /**
     * @notice Change the number of points for Donaswap Profile.
     * @dev Only callable by owner.
     */
    function changeNumberPoints(uint8 _flammyId, uint256 _numberPoints) external onlyOwner validNftId(_flammyId) {
        numberPoints[_flammyId] = _numberPoints;
        emit NewNumberPoints(_flammyId, _numberPoints);
    }

    /**
     * @notice Change the start and final round of the lottery.
     * @dev Only callable by owner.
     */
    function changeLotteryRounds(uint256 _startLotteryRound, uint256 _finalLotteryRound) external onlyOwner {
        require(_startLotteryRound < _finalLotteryRound, "Round: startLotteryRound > finalLotteryRound");
        startLotteryRound = _startLotteryRound;
        finalLotteryRound = _finalLotteryRound;
        emit NewLotteryRounds(_startLotteryRound, _finalLotteryRound);
    }

    /**
     * @notice Change the token uri of a nft
     * @dev Only callable by owner.
     */
    function changeTokenURI(uint8 _flammyId, string calldata _tokenURI) external onlyOwner validNftId(_flammyId) {
        tokenURIs[_flammyId] = _tokenURI;
        emit NewTokenURI(_flammyId, _tokenURI);
    }

    /**
     * @notice Whitelist a user address. Whitelisted address can claim the NFT 3.
     * @dev Only callable by owner.
     */
    function whitelistAddresses(address[] calldata _users) external onlyOwner {
        for (uint256 i = 0; i < _users.length; i++) {
            userWhitelistForNft3[_users[i]] = true;
        }
        emit NewAddressWhitelisted(_users);
    }

    /*** Internal ***/

    /**
     * @notice Check if a user can claim.
     * @dev In order to reduce the gas spent during the minting, this function takes a lotteryId (avoid looping on all the lotteries),
            and a cursor (avoid looping on all the user tickets for a specific lottery). Theses info are easily 
            accessible by the FE.
     * @param _lotteryId Id of the lottery to check against
     * @param _cursor Cursor position of ticket to check against
     */
    function _canClaim(
        address _userAddress,
        uint8 _flammyId,
        uint256 _lotteryId,
        uint256 _cursor
    ) internal view returns (bool) {
        // Common requirements for being able to claim any NFT
        if (
            hasClaimed[_userAddress][_flammyId] ||
            !donaswapProfile.getUserStatus(_userAddress) ||
            block.number >= endBlock ||
            _lotteryId < startLotteryRound ||
            _lotteryId > finalLotteryRound
        ) {
            return false;
        }

        if (_flammyId == nftId1) {
            uint256 size;
            (, , , size) = donaswapLottery.viewUserInfoForLotteryId(_userAddress, _lotteryId, 0, 1);
            return size > 0;
        }
        if (_flammyId == nftId2) {
            bool[] memory ticketStatuses;
            uint256 size;

            (, , ticketStatuses, size) = donaswapLottery.viewUserInfoForLotteryId(
                _userAddress,
                _lotteryId,
                _cursor,
                1
            );

            return size > 0 && ticketStatuses[0];
        }
        if (_flammyId == nftId3) {
            return userWhitelistForNft3[_userAddress];
        }
    }
}
