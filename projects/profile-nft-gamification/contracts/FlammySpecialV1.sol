// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "bsc-library/contracts/IBEP20.sol";
import "bsc-library/contracts/SafeBEP20.sol";

import "./FlammyMintingStation.sol";
import "./DonaswapProfile.sol";

/** @title FlammySpecialV1.
 * @notice It is a contract for users to mint exclusive NFTs
 * based on a FLAME price and userId.
 */
contract FlammySpecialV1 is Ownable {
    using SafeBEP20 for IBEP20;
    using SafeMath for uint256;

    FlammyMintingStation public flammyMintingStation;
    DonaswapProfile public donaswapProfile;

    IBEP20 public flameToken;

    uint256 public maxViewLength;
    uint256 public numberDifferentFlammies;

    // Map if address for a flammyId has already claimed a NFT
    mapping(address => mapping(uint8 => bool)) public hasClaimed;

    // Map if flammyId to its characteristics
    mapping(uint8 => Flammies) public flammyCharacteristics;

    // Number of previous series (i.e. different visuals)
    uint8 private constant previousNumberFlammyIds = 10;

    struct Flammies {
        string tokenURI; // e.g. ipfsHash/hiccups.json
        uint256 thresholdUser; // e.g. 1900 or 100000
        uint256 flameCost;
        bool isActive;
        bool isCreated;
    }

    // Event to notify a new flammy is mintable
    event FlammyAdd(uint8 indexed flammyId, uint256 thresholdUser, uint256 costFlame);

    // Event to notify one of the flammies' requirements to mint differ
    event FlammyChange(uint8 indexed flammyId, uint256 thresholdUser, uint256 costFlame, bool isActive);

    // Event to notify when NFT is successfully minted
    event FlammyMint(address indexed to, uint256 indexed tokenId, uint8 indexed flammyId);

    constructor(
        FlammyMintingStation _flammyMintingStation,
        IBEP20 _flameToken,
        DonaswapProfile _donaswapProfile,
        uint256 _maxViewLength
    ) public {
        flammyMintingStation = _flammyMintingStation;
        flameToken = _flameToken;
        donaswapProfile = _donaswapProfile;
        maxViewLength = _maxViewLength;
    }

    /**
     * @dev Mint NFTs from the FlammyMintingStation contract.
     * Users can claim once.
     */
    function mintNFT(uint8 _flammyId) external {
        // Check that the _flammyId is within boundary
        require(_flammyId >= previousNumberFlammyIds, "ERR_ID_LOW");
        require(flammyCharacteristics[_flammyId].isActive, "ERR_ID_INVALID");

        address senderAddress = _msgSender();

        // 1. Check _msgSender() has not claimed
        require(!hasClaimed[senderAddress][_flammyId], "ERR_HAS_CLAIMED");

        uint256 userId;
        bool isUserActive;

        (userId, , , , , isUserActive) = donaswapProfile.getUserProfile(senderAddress);

        require(userId < flammyCharacteristics[_flammyId].thresholdUser, "ERR_USER_NOT_ELIGIBLE");

        require(isUserActive, "ERR_USER_NOT_ACTIVE");

        // Check if there is any cost associated with getting the flammy
        if (flammyCharacteristics[_flammyId].flameCost > 0) {
            flameToken.safeTransferFrom(senderAddress, address(this), flammyCharacteristics[_flammyId].flameCost);
        }

        // Update that _msgSender() has claimed
        hasClaimed[senderAddress][_flammyId] = true;

        uint256 tokenId = flammyMintingStation.mintCollectible(
            senderAddress,
            flammyCharacteristics[_flammyId].tokenURI,
            _flammyId
        );

        emit FlammyMint(senderAddress, tokenId, _flammyId);
    }

    function addFlammy(
        uint8 _flammyId,
        string calldata _tokenURI,
        uint256 _thresholdUser,
        uint256 _flameCost
    ) external onlyOwner {
        require(!flammyCharacteristics[_flammyId].isCreated, "ERR_CREATED");
        require(_flammyId >= previousNumberFlammyIds, "ERR_ID_LOW_2");

        flammyCharacteristics[_flammyId] = Flammies({
            tokenURI: _tokenURI,
            thresholdUser: _thresholdUser,
            flameCost: _flameCost,
            isActive: true,
            isCreated: true
        });

        numberDifferentFlammies = numberDifferentFlammies.add(1);

        emit FlammyAdd(_flammyId, _thresholdUser, _flameCost);
    }

    /**
     * @dev It transfers the FLAME tokens back to the chef address.
     * Only callable by the owner.
     */
    function claimFee(uint256 _amount) external onlyOwner {
        flameToken.safeTransfer(_msgSender(), _amount);
    }

    function updateFlammy(
        uint8 _flammyId,
        uint256 _thresholdUser,
        uint256 _flameCost,
        bool _isActive
    ) external onlyOwner {
        require(flammyCharacteristics[_flammyId].isCreated, "ERR_NOT_CREATED");
        flammyCharacteristics[_flammyId].thresholdUser = _thresholdUser;
        flammyCharacteristics[_flammyId].flameCost = _flameCost;
        flammyCharacteristics[_flammyId].isActive = _isActive;

        emit FlammyChange(_flammyId, _thresholdUser, _flameCost, _isActive);
    }

    function updateMaxViewLength(uint256 _newMaxViewLength) external onlyOwner {
        maxViewLength = _newMaxViewLength;
    }

    function canClaimSingle(address _userAddress, uint8 _flammyId) external view returns (bool) {
        if (!donaswapProfile.hasRegistered(_userAddress)) {
            return false;
        } else {
            uint256 userId;
            bool userStatus;

            (userId, , , , , userStatus) = donaswapProfile.getUserProfile(_userAddress);

            if (!userStatus) {
                return false;
            } else {
                bool claimStatus = _canClaim(_userAddress, userId, _flammyId);
                return claimStatus;
            }
        }
    }

    function canClaimMultiple(address _userAddress, uint8[] calldata _flammyIds) external view returns (bool[] memory) {
        require(_flammyIds.length <= maxViewLength, "ERR_LENGTH_VIEW");

        if (!donaswapProfile.hasRegistered(_userAddress)) {
            bool[] memory responses = new bool[](0);
            return responses;
        } else {
            uint256 userId;
            bool userStatus;

            (userId, , , , , userStatus) = donaswapProfile.getUserProfile(_userAddress);

            if (!userStatus) {
                bool[] memory responses = new bool[](0);
                return responses;
            } else {
                bool[] memory responses = new bool[](_flammyIds.length);

                for (uint256 i = 0; i < _flammyIds.length; i++) {
                    bool claimStatus = _canClaim(_userAddress, userId, _flammyIds[i]);
                    responses[i] = claimStatus;
                }
                return responses;
            }
        }
    }

    /**
     * @dev Check if user can claim.
     * If the address hadn't set up a profile, it will return an error.
     */
    function _canClaim(
        address _userAddress,
        uint256 userId,
        uint8 _flammyId
    ) internal view returns (bool) {
        uint256 flammyThreshold = flammyCharacteristics[_flammyId].thresholdUser;
        bool flammyActive = flammyCharacteristics[_flammyId].isActive;

        if (hasClaimed[_userAddress][_flammyId]) {
            return false;
        } else if (!flammyActive) {
            return false;
        } else if (userId >= flammyThreshold) {
            return false;
        } else {
            return true;
        }
    }
}
