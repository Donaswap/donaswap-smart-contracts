# DonaswapFlammies (WIP)

This document explains how the `DonaswapFlammies` contract works.

## 1. Variables

### Public variables

#### Mappings

- `flammyCount`: checks how many flammies are deployed per `flammyId`
- `flammyBurnCount`: checks how many were burnt per `flammyId`

#### String

- `baseURI`: "ipfs://"
- `name`: "Donaswap Flammies"
- `symbol`: "PB"

### Private variables

_(TODO)_

## 2. Functions

### Public functions

#### View functions

- `getFlammyId(_tokenId)`: it returns the flammyId for a `_tokenId`
- `getFlammyName(_flammyId)`: it returns the flammy name (string) for a `_flammyId`
- `getFlammyNameOfTokenId(_tokenId)`: it returns the flammy name (string) for a `_tokenId`

#### Inherited view functions

- `balanceOf(owner)`: checks how many NFTs an address has
- `ownerOf(tokenId)`: checks who is the owner of a tokenId
- `tokenOfOwnerByIndex(owner, index)`: checks who is the owner of the first NFT owned by an address (use with balanceOf)
- `tokenByIndex(index)`: checks the tokenId for each token (use with totalSupply) (NOTE: not useful for FE)
- `totalSupply()`: returns how many tokens exist (excl. burnt)

#### User functions (inherited)

- `safeTransferFrom(from, to, tokenId)`:
- `transferFrom(from, to, tokenId)`
- `approve(to, tokenId)`
- `getApproved(tokenId)`
- `setApprovalForAll(operator, _approved)`
- `isApprovedForAll(owner, operator)`
- `safeTransferFrom(from, to, tokenId, data)`

### 2.2 Private functions

_TODO_

## 3. Events

Note: all these events are inherited.

- `Approval(owner, approved, tokenId)`
- `ApprovalForAll(owner, operator, approved)`
- `Transfer(from, to, tokenId)`
