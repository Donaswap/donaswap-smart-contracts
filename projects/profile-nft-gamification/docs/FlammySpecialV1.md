# FlammySpecialV1

This document explains how the `FlammySpecialV1` contract works.

`FlammySpecialV1` inherits from [`Ownable`](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Owanable.sol). Hence, it also inherits from [`Context`](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/Context.sol).

## 1. Variables

### Public variables

#### Numeric variables (uint256)

- `maxViewLength`: maximum length parameter for view function
- `numberDifferentFlammies`: keeps track of number of flammies available

#### Mappings

- `flammyCharacteristics`: maps a`flammyId` to its characteristics
- `hasClaimed`: checks whether a user has claimed for a specific `flammyId`.

### Private variables

#### Numeric variables (uint8)

- `previousNumberFlammyIds`: number of series before (i.e., 10)

## 2. Functions

Note: all functions that are not inherited are external.

### Users

- `mintNFT(_tokenId)`: mint a new NFT (with `_tokenId` from 10+). It is only possible if the user has NOT claimed in V2 or V3 (i.e. this contract).

### Owner

- `addFlammy(uint8 _flammyId, string calldata _tokenURI, uint256 _thresholdUser, uint256 _flameCost )`: add a new flammy with its associated characteristics
- `claimFee(uint256 _amount)`: retrieve the FLAME token from the contract
- `updateFlammy( uint8 _flammyId, uint256 _thresholdUser, uint256 _flameCost, bool _isActive )`: update the flammy's characteristics (only the tokenURI is immutable)
- `updateMaxViewLength(uint256 _newMaxViewLength)`: update the `maxViewLength` for view functions

### View

- `canClaimSingle(address _userAddress, uint8 _flammyId) returns (bool)`: it checks whether an address can mint a specific flammy (based on `_flammyId`)
- `canClaimMultiple(address _userAddress, uint8[] calldata _flammyIds) external view returns (bool[] memory)`: it checks whether an address can mint a specific flammy (based on `_flammyIds[]`). If address has no active profile, it returns an empty array. If the user has never registered, it reverts.

## 3. Events

### User-related

```
event FlammyMint(
    address indexed to,
    uint256 indexed tokenId,
    uint8 indexed flammyId
);
```

It indicates when a new flammy is minted.

### Admin-related

```
event FlammyAdd(
    uint8 indexed flammyId,
    uint256 thresholdUser,
    uint256 costFlame
);
```

It notifies that a new flammy was added for minting.

```
event FlammyChange(
    uint8 indexed flammyId,
    uint256 thresholdUser,
    uint256 costFlame,
    bool isActive
);
```

It notifies that one of the flammies' requirements to mint has changed.
