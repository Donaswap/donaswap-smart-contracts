import { ethers, network } from "hardhat";
import config from "../../config";

const currentNetwork = network.name;

const main = async () => {
  console.log("Deploying to network:", currentNetwork);

  const _thresholdRound = "";
  const _endBlock = "";
  const _numberPoints = "";
  const _campaignId = "";
  const _tokenURI = "";

  const FlammySpecialPrediction = await ethers.getContractFactory("FlammySpecialPrediction");

  const flammySpecialPrediction = await FlammySpecialPrediction.deploy(
    config.FlameVault[currentNetwork],
    config.FlammyMintingStation[currentNetwork],
    config.DonaswapProfile[currentNetwork],
    _endBlock,
    _thresholdRound,
    _numberPoints,
    _campaignId,
    _tokenURI
  );

  await flammySpecialPrediction.deployed();
  console.log("FlammySpecialPrediction deployed to:", flammySpecialPrediction.address);
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
