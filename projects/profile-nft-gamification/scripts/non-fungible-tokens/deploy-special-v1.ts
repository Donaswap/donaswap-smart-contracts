import { ethers, network } from "hardhat";
import config from "../../config";

const currentNetwork = network.name;

const main = async () => {
  console.log("Deploying to network:", currentNetwork);

  const _ipfsURIFlammy10 = "";
  const _ipfsURIFlammy11 = "";
  const _maxViewLength = "10";
  const _thresholdFlammy10 = "";
  const _thresholdFlammy11 = "";

  const FlammySpecialV1 = await ethers.getContractFactory("FlammySpecialV1");

  const flammySpecialV1 = await FlammySpecialV1.deploy(
    config.FlammyMintingStation[currentNetwork],
    config.FlameToken[currentNetwork],
    config.DonaswapProfile[currentNetwork],
    _maxViewLength
  );

  await flammySpecialV1.deployed();
  console.log("FlammySpecialV1 deployed to:", flammySpecialV1.address);

  await flammySpecialV1.addFlammy("10", _ipfsURIFlammy10, _thresholdFlammy10, "0");
  await flammySpecialV1.addFlammy("11", _ipfsURIFlammy11, _thresholdFlammy11, "0");
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
