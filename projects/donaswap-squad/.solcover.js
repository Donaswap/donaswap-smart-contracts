module.exports = {
  skipFiles: [
    "interfaces/IDonaswapProfile.sol",
    "test/MockERC20.sol",
    "test/MockERC721.sol",
    "test/MockDonaswapProfile.sol",
  ],
  measureStatementCoverage: true,
  measureFunctionCoverage: true,
};
