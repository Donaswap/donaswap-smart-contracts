module.exports = {
  skipFiles: [
    "interfaces/IDonaswapFlammies.sol",
    "interfaces/IWETH.sol",
    "test/MockERC20.sol",
    "test/MockNFT.sol",
    "test/DonaswapFlammies.sol",
    "test/WBNB.sol",
  ],
  measureStatementCoverage: true,
  measureFunctionCoverage: true,
};
