// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "bsc-library/contracts/IBEP20.sol";
import "bsc-library/contracts/SafeBEP20.sol";

import "./archive/FlammyFactoryV2.sol";
import "./FlammyMintingStation.sol";

/** @title FlammyFactoryV3.
 * @notice It is a contract for users to mint 'starter NFTs'.
 */
contract FlammyFactoryV3 is Ownable {
    using SafeMath for uint256;
    using SafeBEP20 for IBEP20;

    FlammyFactoryV2 public flammyFactoryV2;
    FlammyMintingStation public flammyMintingStation;

    IBEP20 public flameToken;

    // starting block
    uint256 public startBlockNumber;

    // Number of FLAMEs a user needs to pay to acquire a token
    uint256 public tokenPrice;

    // Map if address has already claimed a NFT
    mapping(address => bool) public hasClaimed;

    // IPFS hash for new json
    string private ipfsHash;

    // number of total series (i.e. different visuals)
    uint8 private constant numberFlammyIds = 10;

    // number of previous series (i.e. different visuals)
    uint8 private constant previousNumberFlammyIds = 5;

    // Map the token number to URI
    mapping(uint8 => string) private flammyIdURIs;

    // Event to notify when NFT is successfully minted
    event FlammyMint(address indexed to, uint256 indexed tokenId, uint8 indexed flammyId);

    /**
     * @dev
     */
    constructor(
        FlammyFactoryV2 _flammyFactoryV2,
        FlammyMintingStation _flammyMintingStation,
        IBEP20 _flameToken,
        uint256 _tokenPrice,
        string memory _ipfsHash,
        uint256 _startBlockNumber
    ) public {
        flammyFactoryV2 = _flammyFactoryV2;
        flammyMintingStation = _flammyMintingStation;
        flameToken = _flameToken;
        tokenPrice = _tokenPrice;
        ipfsHash = _ipfsHash;
        startBlockNumber = _startBlockNumber;
    }

    /**
     * @dev Mint NFTs from the FlammyMintingStation contract.
     * Users can specify what flammyId they want to mint. Users can claim once.
     */
    function mintNFT(uint8 _flammyId) external {
        address senderAddress = _msgSender();

        bool hasClaimedV2 = flammyFactoryV2.hasClaimed(senderAddress);

        // Check if _msgSender() has claimed in previous factory
        require(!hasClaimedV2, "Has claimed in v2");
        // Check _msgSender() has not claimed
        require(!hasClaimed[senderAddress], "Has claimed");
        // Check block time is not too late
        require(block.number > startBlockNumber, "too early");
        // Check that the _flammyId is within boundary:
        require(_flammyId >= previousNumberFlammyIds, "flammyId too low");
        // Check that the _flammyId is within boundary:
        require(_flammyId < numberFlammyIds, "flammyId too high");

        // Update that _msgSender() has claimed
        hasClaimed[senderAddress] = true;

        // Send FLAME tokens to this contract
        flameToken.safeTransferFrom(senderAddress, address(this), tokenPrice);

        string memory tokenURI = flammyIdURIs[_flammyId];

        uint256 tokenId = flammyMintingStation.mintCollectible(senderAddress, tokenURI, _flammyId);

        emit FlammyMint(senderAddress, tokenId, _flammyId);
    }

    /**
     * @dev It transfers the FLAME tokens back to the chef address.
     * Only callable by the owner.
     */
    function claimFee(uint256 _amount) external onlyOwner {
        flameToken.safeTransfer(_msgSender(), _amount);
    }

    /**
     * @dev Set up json extensions for flammies 5-9
     * Assign tokenURI to look for each flammyId in the mint function
     * Only the owner can set it.
     */
    function setFlammyJson(
        string calldata _flammyId5Json,
        string calldata _flammyId6Json,
        string calldata _flammyId7Json,
        string calldata _flammyId8Json,
        string calldata _flammyId9Json
    ) external onlyOwner {
        flammyIdURIs[5] = string(abi.encodePacked(ipfsHash, _flammyId5Json));
        flammyIdURIs[6] = string(abi.encodePacked(ipfsHash, _flammyId6Json));
        flammyIdURIs[7] = string(abi.encodePacked(ipfsHash, _flammyId7Json));
        flammyIdURIs[8] = string(abi.encodePacked(ipfsHash, _flammyId8Json));
        flammyIdURIs[9] = string(abi.encodePacked(ipfsHash, _flammyId9Json));
    }

    /**
     * @dev Allow to set up the start number
     * Only the owner can set it.
     */
    function setStartBlockNumber(uint256 _newStartBlockNumber) external onlyOwner {
        require(_newStartBlockNumber > block.number, "too short");
        startBlockNumber = _newStartBlockNumber;
    }

    /**
     * @dev Allow to change the token price
     * Only the owner can set it.
     */
    function updateTokenPrice(uint256 _newTokenPrice) external onlyOwner {
        tokenPrice = _newTokenPrice;
    }

    function canMint(address userAddress) external view returns (bool) {
        if ((hasClaimed[userAddress]) || (flammyFactoryV2.hasClaimed(userAddress))) {
            return false;
        } else {
            return true;
        }
    }
}
