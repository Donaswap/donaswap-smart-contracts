// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "bsc-library/contracts/IBEP20.sol";

import "bsc-library/contracts/SafeBEP20.sol";

import "../DonaswapFlammies.sol";

contract FlammyFactoryV2 is Ownable {
    using SafeMath for uint256;
    using SafeBEP20 for IBEP20;

    DonaswapFlammies public donaswapFlammies;
    IBEP20 public flameToken;

    // end block number to get collectibles
    uint256 public endBlockNumber;

    // starting block
    uint256 public startBlockNumber;

    // Number of FLAMEs a user needs to pay to acquire a token
    uint256 public tokenPrice;

    // Map if address has already claimed a NFT
    mapping(address => bool) public hasClaimed;

    // IPFS hash for new json
    string private ipfsHash;

    // number of total series (i.e. different visuals)
    uint8 private constant numberFlammyIds = 10;

    // number of previous series (i.e. different visuals)
    uint8 private constant previousNumberFlammyIds = 5;

    // Map the token number to URI
    mapping(uint8 => string) private flammyIdURIs;

    // Event to notify when NFT is successfully minted
    event FlammyMint(address indexed to, uint256 indexed tokenId, uint8 indexed flammyId);

    /**
     * @dev A maximum number of NFT tokens that is distributed by this contract
     * is defined as totalSupplyDistributed.
     */
    constructor(
        DonaswapFlammies _donaswapFlammies,
        IBEP20 _flameToken,
        uint256 _tokenPrice,
        string memory _ipfsHash,
        uint256 _startBlockNumber,
        uint256 _endBlockNumber
    ) public {
        donaswapFlammies = _donaswapFlammies;
        flameToken = _flameToken;
        tokenPrice = _tokenPrice;
        ipfsHash = _ipfsHash;
        startBlockNumber = _startBlockNumber;
        endBlockNumber = _endBlockNumber;
    }

    /**
     * @dev Mint NFTs from the DonaswapFlammies contract.
     * Users can specify what flammyId they want to mint. Users can claim once.
     * There is a limit on how many are distributed. It requires FLAME balance to be > 0.
     */
    function mintNFT(uint8 _flammyId) external {
        // Check _msgSender() has not claimed
        require(!hasClaimed[_msgSender()], "Has claimed");
        // Check block time is not too late
        require(block.number > startBlockNumber, "too early");
        // Check block time is not too late
        require(block.number < endBlockNumber, "too late");
        // Check that the _flammyId is within boundary:
        require(_flammyId >= previousNumberFlammyIds, "flammyId too low");
        // Check that the _flammyId is within boundary:
        require(_flammyId < numberFlammyIds, "flammyId too high");

        // Update that _msgSender() has claimed
        hasClaimed[_msgSender()] = true;

        // Send FLAME tokens to this contract
        flameToken.safeTransferFrom(address(_msgSender()), address(this), tokenPrice);

        string memory tokenURI = flammyIdURIs[_flammyId];

        uint256 tokenId = donaswapFlammies.mint(address(_msgSender()), tokenURI, _flammyId);

        emit FlammyMint(_msgSender(), tokenId, _flammyId);
    }

    /**
     * @dev It transfers the ownership of the NFT contract
     * to a new address.
     */
    function changeOwnershipNFTContract(address _newOwner) external onlyOwner {
        donaswapFlammies.transferOwnership(_newOwner);
    }

    /**
     * @dev It transfers the FLAME tokens back to the chef address.
     * Only callable by the owner.
     */
    function claimFee(uint256 _amount) external onlyOwner {
        flameToken.safeTransfer(_msgSender(), _amount);
    }

    /**
     * @dev Set up json extensions for flammies 5-9
     * Assign tokenURI to look for each flammyId in the mint function
     * Only the owner can set it.
     */
    function setFlammyJson(
        string calldata _flammyId5Json,
        string calldata _flammyId6Json,
        string calldata _flammyId7Json,
        string calldata _flammyId8Json,
        string calldata _flammyId9Json
    ) external onlyOwner {
        flammyIdURIs[5] = string(abi.encodePacked(ipfsHash, _flammyId5Json));
        flammyIdURIs[6] = string(abi.encodePacked(ipfsHash, _flammyId6Json));
        flammyIdURIs[7] = string(abi.encodePacked(ipfsHash, _flammyId7Json));
        flammyIdURIs[8] = string(abi.encodePacked(ipfsHash, _flammyId8Json));
        flammyIdURIs[9] = string(abi.encodePacked(ipfsHash, _flammyId9Json));
    }

    /**
     * @dev Set up names for flammies 5-9
     * Only the owner can set it.
     */
    function setFlammyNames(
        string calldata _flammyId5,
        string calldata _flammyId6,
        string calldata _flammyId7,
        string calldata _flammyId8,
        string calldata _flammyId9
    ) external onlyOwner {
        donaswapFlammies.setFlammyName(5, _flammyId5);
        donaswapFlammies.setFlammyName(6, _flammyId6);
        donaswapFlammies.setFlammyName(7, _flammyId7);
        donaswapFlammies.setFlammyName(8, _flammyId8);
        donaswapFlammies.setFlammyName(9, _flammyId9);
    }

    /**
     * @dev Allow to set up the start number
     * Only the owner can set it.
     */
    function setStartBlockNumber(uint256 _newStartBlockNumber) external onlyOwner {
        require(_newStartBlockNumber > block.number, "too short");
        startBlockNumber = _newStartBlockNumber;
    }

    /**
     * @dev Allow to set up the end block number
     * Only the owner can set it.
     */
    function setEndBlockNumber(uint256 _newEndBlockNumber) external onlyOwner {
        require(_newEndBlockNumber > block.number, "too short");
        require(_newEndBlockNumber > startBlockNumber, "must be > startBlockNumber");
        endBlockNumber = _newEndBlockNumber;
    }

    /**
     * @dev Allow to change the token price
     * Only the owner can set it.
     */
    function updateTokenPrice(uint256 _newTokenPrice) external onlyOwner {
        tokenPrice = _newTokenPrice;
    }
}
