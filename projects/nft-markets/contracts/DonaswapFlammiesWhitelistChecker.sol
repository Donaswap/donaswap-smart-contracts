// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";

import {ICollectionWhitelistChecker} from "./interfaces/ICollectionWhitelistChecker.sol";
import {IDonaswapFlammies} from "./interfaces/IDonaswapFlammies.sol";

contract DonaswapFlammiesWhitelistChecker is Ownable, ICollectionWhitelistChecker {
    IDonaswapFlammies public donaswapFlammies;

    mapping(uint8 => bool) public isFlammyIdRestricted;

    event NewRestriction(uint8[] flammyIds);
    event RemoveRestriction(uint8[] flammyIds);

    /**
     * @notice Constructor
     * @param _donaswapFlammiesAddress: DonaswapFlammies contract
     */
    constructor(address _donaswapFlammiesAddress) {
        donaswapFlammies = IDonaswapFlammies(_donaswapFlammiesAddress);
    }

    /**
     * @notice Restrict tokens with specific flammyIds to be sold
     * @param _flammyIds: flammyIds to restrict for trading on the market
     */
    function addRestrictionForFlammies(uint8[] calldata _flammyIds) external onlyOwner {
        for (uint8 i = 0; i < _flammyIds.length; i++) {
            require(!isFlammyIdRestricted[_flammyIds[i]], "Operations: Already restricted");
            isFlammyIdRestricted[_flammyIds[i]] = true;
        }

        emit NewRestriction(_flammyIds);
    }

    /**
     * @notice Remove restrictions tokens with specific flammyIds to be sold
     * @param _flammyIds: flammyIds to restrict for trading on the market
     */
    function removeRestrictionForFlammies(uint8[] calldata _flammyIds) external onlyOwner {
        for (uint8 i = 0; i < _flammyIds.length; i++) {
            require(isFlammyIdRestricted[_flammyIds[i]], "Operations: Not restricted");
            isFlammyIdRestricted[_flammyIds[i]] = false;
        }

        emit RemoveRestriction(_flammyIds);
    }

    /**
     * @notice Check whether token can be listed
     * @param _tokenId: tokenId of the NFT to list
     */
    function canList(uint256 _tokenId) external view override returns (bool) {
        uint8 flammyId = donaswapFlammies.getFlammyId(_tokenId);

        return !isFlammyIdRestricted[flammyId];
    }
}
