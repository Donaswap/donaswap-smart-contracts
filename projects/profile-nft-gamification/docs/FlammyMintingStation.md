# FlammyMintingStation

This document explains how the `FlammyMintingStation` contract works. `FlammyMintingStation` is the owner of `DonaswapFlammies` and can grant minter roles to other contracts who can mint new collectibles.

`FlammyMintingStation` inherits from [`AccessControl`](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/AccessControl.sol). Hence, it also inherits from [`Context`](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/Context.sol).

## 1. Variables

### Public variables

#### Bytes32 variables (uint256)

- `DEFAULT_ADMIN_ROLE`: bytes32 of admin role
- `MINTER_ROLE`: bytes32 of role for minting new NFTs

## 2. Functions

Note: all functions that are not inherited are external.

### Minter functions

- `mintCollectible(address _tokenReceiver, string _tokenURI, uint8 _flammyId)`: mint a new NFT with tokenURI to a receiver address.

### Admin functions

- `setFlammyName(uint8 _flammyId, string calldata _flammyName)`: set the flammyName in the `DonaswapFlammies` contract
- `changeOwnershipNFTContract(_newOwner)`: change the owner of `DonaswapFlammies`
