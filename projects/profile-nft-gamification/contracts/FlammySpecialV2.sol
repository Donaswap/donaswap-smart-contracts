// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "bsc-library/contracts/IBEP20.sol";
import "bsc-library/contracts/SafeBEP20.sol";

import "./FlammyMintingStation.sol";
import "./DonaswapProfile.sol";

/** @title FlammySpecialV2.
 * @notice It is a contract for users to mint exclusive Easter
 * collectibles for their teams.
 */
contract FlammySpecialV2 is Ownable {
    using SafeBEP20 for IBEP20;
    using SafeMath for uint256;

    FlammyMintingStation public flammyMintingStation;
    DonaswapProfile public donaswapProfile;

    IBEP20 public flameToken;

    uint8 public constant previousNumberFlammyIds = 12;

    uint256 public endBlock;
    uint256 public thresholdUser;

    // Map if flammyId to its tokenURI
    mapping(uint8 => string) public flammyTokenURI;

    // Map if address has already claimed a NFT
    mapping(address => bool) public hasClaimed;

    // Map teamId to its flammyId
    mapping(uint256 => uint8) public teamIdToFlammyId;

    event FlammyAdd(uint8 flammyId, uint256 teamId);

    // Event to notify when NFT is successfully minted
    event FlammyMint(address indexed to, uint256 indexed tokenId, uint8 indexed flammyId);

    event NewEndBlock(uint256 endBlock);
    event NewThresholdUser(uint256 thresholdUser);

    constructor(
        FlammyMintingStation _flammyMintingStation,
        IBEP20 _flameToken,
        DonaswapProfile _donaswapProfile,
        uint256 _thresholdUser,
        uint256 _endBlock
    ) public {
        flammyMintingStation = _flammyMintingStation;
        flameToken = _flameToken;
        donaswapProfile = _donaswapProfile;
        thresholdUser = _thresholdUser;
        endBlock = _endBlock;
    }

    /**
     * @notice Mint a NFT from the FlammyMintingStation contract.
     * @dev Users can claim once. It maps to the teamId.
     */
    function mintNFT() external {
        require(block.number < endBlock, "TOO_LATE");

        address senderAddress = _msgSender();

        // Check _msgSender() has not claimed
        require(!hasClaimed[senderAddress], "ERR_HAS_CLAIMED");

        uint256 userId;
        uint256 userTeamId;
        bool isUserActive;

        (userId, , userTeamId, , , isUserActive) = donaswapProfile.getUserProfile(senderAddress);

        require(userId < thresholdUser, "ERR_USER_NOT_ELIGIBLE");
        require(isUserActive, "ERR_USER_NOT_ACTIVE");

        // Update that _msgSender() has claimed
        hasClaimed[senderAddress] = true;

        uint8 flammyId = teamIdToFlammyId[userTeamId];

        require(flammyId >= previousNumberFlammyIds, "NOT_VALID");

        string memory tokenURI = flammyTokenURI[flammyId];

        uint256 tokenId = flammyMintingStation.mintCollectible(senderAddress, tokenURI, flammyId);

        emit FlammyMint(senderAddress, tokenId, flammyId);
    }

    /**
     * @notice Add/modify flammyId for a teamId and metadata
     * @dev Only callable by owner.
     */
    function addFlammy(
        uint8 _flammyId,
        uint256 _teamId,
        string calldata _tokenURI
    ) external onlyOwner {
        require(_flammyId >= previousNumberFlammyIds, "ERR_ID_LOW_2");

        teamIdToFlammyId[_teamId] = _flammyId;
        flammyTokenURI[_flammyId] = _tokenURI;

        emit FlammyAdd(_flammyId, _teamId);
    }

    /**
     * @notice Change end block for distribution
     * @dev Only callable by owner.
     */
    function changeEndBlock(uint256 _endBlock) external onlyOwner {
        endBlock = _endBlock;
        emit NewEndBlock(_endBlock);
    }

    /**
     * @notice Change user threshold
     * @dev Only callable by owner.
     */
    function changeThresholdUser(uint256 _thresholdUser) external onlyOwner {
        thresholdUser = _thresholdUser;
        emit NewThresholdUser(_thresholdUser);
    }

    /**
     * @notice Check if a user can claim.
     */
    function canClaim(address _userAddress) external view returns (bool) {
        if (hasClaimed[_userAddress]) {
            return false;
        } else {
            if (!donaswapProfile.getUserStatus(_userAddress)) {
                return false;
            } else {
                uint256 userId;
                (userId, , , , , ) = donaswapProfile.getUserProfile(_userAddress);

                if (userId < thresholdUser) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}
