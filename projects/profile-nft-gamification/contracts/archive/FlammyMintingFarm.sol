// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "bsc-library/contracts/IBEP20.sol";
import "bsc-library/contracts/SafeBEP20.sol";

import "../DonaswapFlammies.sol";

contract FlammyMintingFarm is Ownable {
    using SafeMath for uint8;
    using SafeMath for uint256;

    using SafeBEP20 for IBEP20;

    DonaswapFlammies public donaswapFlammies;
    IBEP20 public flameToken;

    // Map if address can claim a NFT
    mapping(address => bool) public canClaim;

    // Map if address has already claimed a NFT
    mapping(address => bool) public hasClaimed;

    // starting block
    uint256 public startBlockNumber;

    // end block number to claim FLAMEs by burning NFT
    uint256 public endBlockNumber;

    // number of total flammies burnt
    uint256 public countFlammiesBurnt;

    // Number of FLAMEs a user can collect by burning her NFT
    uint256 public flamePerBurn;

    // current distributed number of NFTs
    uint256 public currentDistributedSupply;

    // number of total NFTs distributed
    uint256 public totalSupplyDistributed;

    // baseURI (on IPFS)
    string private baseURI;

    // Map the token number to URI
    mapping(uint8 => string) private flammyIdURIs;

    // number of initial series (i.e. different visuals)
    uint8 private numberOfFlammyIds;

    // Event to notify when NFT is successfully minted
    event FlammyMint(address indexed to, uint256 indexed tokenId, uint8 indexed flammyId);

    // Event to notify when NFT is successfully minted
    event FlammyBurn(address indexed from, uint256 indexed tokenId);

    /**
     * @dev A maximum number of NFT tokens that is distributed by this contract
     * is defined as totalSupplyDistributed.
     */
    constructor(
        IBEP20 _flameToken,
        uint256 _totalSupplyDistributed,
        uint256 _flamePerBurn,
        string memory _baseURI,
        string memory _ipfsHash,
        uint256 _endBlockNumber
    ) public {
        donaswapFlammies = new DonaswapFlammies(_baseURI);
        flameToken = _flameToken;
        totalSupplyDistributed = _totalSupplyDistributed;
        flamePerBurn = _flamePerBurn;
        baseURI = _baseURI;
        endBlockNumber = _endBlockNumber;

        // Other parameters initialized
        numberOfFlammyIds = 5;

        // Assign tokenURI to look for each flammyId in the mint function
        flammyIdURIs[0] = string(abi.encodePacked(_ipfsHash, "swapsies.json"));
        flammyIdURIs[1] = string(abi.encodePacked(_ipfsHash, "drizzle.json"));
        flammyIdURIs[2] = string(abi.encodePacked(_ipfsHash, "blueberries.json"));
        flammyIdURIs[3] = string(abi.encodePacked(_ipfsHash, "circular.json"));
        flammyIdURIs[4] = string(abi.encodePacked(_ipfsHash, "sparkle.json"));

        // Set token names for each flammyId
        donaswapFlammies.setFlammyName(0, "Swapsies");
        donaswapFlammies.setFlammyName(1, "Drizzle");
        donaswapFlammies.setFlammyName(2, "Blueberries");
        donaswapFlammies.setFlammyName(3, "Circular");
        donaswapFlammies.setFlammyName(4, "Sparkle");
    }

    /**
     * @dev Mint NFTs from the DonaswapFlammies contract.
     * Users can specify what flammyId they want to mint. Users can claim once.
     * There is a limit on how many are distributed. It requires FLAME balance to be >0.
     */
    function mintNFT(uint8 _flammyId) external {
        // Check msg.sender can claim
        require(canClaim[msg.sender], "Cannot claim");
        // Check msg.sender has not claimed
        require(hasClaimed[msg.sender] == false, "Has claimed");
        // Check whether it is still possible to mint
        require(currentDistributedSupply < totalSupplyDistributed, "Nothing left");
        // Check whether user owns any FLAME
        require(flameToken.balanceOf(msg.sender) > 0, "Must own FLAME");
        // Check that the _flammyId is within boundary:
        require(_flammyId < numberOfFlammyIds, "flammyId unavailable");
        // Update that msg.sender has claimed
        hasClaimed[msg.sender] = true;

        // Update the currentDistributedSupply by 1
        currentDistributedSupply = currentDistributedSupply.add(1);

        string memory tokenURI = flammyIdURIs[_flammyId];

        uint256 tokenId = donaswapFlammies.mint(address(msg.sender), tokenURI, _flammyId);

        emit FlammyMint(msg.sender, tokenId, _flammyId);
    }

    /**
     * @dev Burn NFT from the DonaswapFlammies contract.
     * Users can burn their NFT to get a set number of FLAME.
     * There is a cap on how many can be distributed for free.
     */
    function burnNFT(uint256 _tokenId) external {
        require(donaswapFlammies.ownerOf(_tokenId) == msg.sender, "Not the owner");
        require(block.number < endBlockNumber, "too late");

        donaswapFlammies.burn(_tokenId);
        countFlammiesBurnt = countFlammiesBurnt.add(1);
        flameToken.safeTransfer(address(msg.sender), flamePerBurn);
        emit FlammyBurn(msg.sender, _tokenId);
    }

    /**
     * @dev Allow to set up the start number
     * Only the owner can set it.
     */
    function setStartBlockNumber() external onlyOwner {
        startBlockNumber = block.number;
    }

    /**
     * @dev Allow the contract owner to whitelist addresses.
     * Only these addresses can claim.
     */
    function whitelistAddresses(address[] calldata users) external onlyOwner {
        for (uint256 i = 0; i < users.length; i++) {
            canClaim[users[i]] = true;
        }
    }

    /**
     * @dev It transfers the FLAME tokens back to the chef address.
     * Only callable by the owner.
     */
    function withdrawFlame(uint256 _amount) external onlyOwner {
        require(block.number >= endBlockNumber, "too early");
        flameToken.safeTransfer(address(msg.sender), _amount);
    }

    /**
     * @dev It transfers the ownership of the NFT contract
     * to a new address.
     */
    function changeOwnershipNFTContract(address _newOwner) external onlyOwner {
        donaswapFlammies.transferOwnership(_newOwner);
    }
}
