import { ethers, network, run } from "hardhat";
import config from "../config";

const main = async () => {
  // Compile contracts
  await run("compile");
  console.log("Compiled contracts.");

  const networkName = network.name;

  // Sanity checks
  if (networkName === "mainnet") {
    if (!process.env.KEY_MAINNET) {
      throw new Error("Missing private key, refer to README 'Deployment' section");
    }
  } else if (networkName === "testnet") {
    if (!process.env.KEY_TESTNET) {
      throw new Error("Missing private key, refer to README 'Deployment' section");
    }
  }

  if (!config.DonaswapRouter[networkName] || config.DonaswapRouter[networkName] === ethers.constants.AddressZero) {
    throw new Error("Missing router address, refer to README 'Deployment' section");
  }

  if (!config.WBNB[networkName] || config.WBNB[networkName] === ethers.constants.AddressZero) {
    throw new Error("Missing WBNB address, refer to README 'Deployment' section");
  }

  console.log("Deploying to network:", networkName);

  // Deploy DonaswapZapV1
  console.log("Deploying DonaswapZap V1..");

  const DonaswapZapV1 = await ethers.getContractFactory("DonaswapZapV1");

  const donaswapZap = await DonaswapZapV1.deploy(
    config.WBNB[networkName],
    config.DonaswapRouter[networkName],
    config.MaxZapReverseRatio[networkName]
  );

  await donaswapZap.deployed();

  console.log("DonaswapZap V1 deployed to:", donaswapZap.address);
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
