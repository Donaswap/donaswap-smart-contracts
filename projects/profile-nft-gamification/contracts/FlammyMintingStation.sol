// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "./DonaswapFlammies.sol";

/** @title FlammyMintingStation.
 * @dev This contract allows different factories to mint
 * Donaswap Collectibles/Flammies.
 */
contract FlammyMintingStation is AccessControl {
    DonaswapFlammies public donaswapFlammies;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    // Modifier for minting roles
    modifier onlyMinter() {
        require(hasRole(MINTER_ROLE, _msgSender()), "Not a minting role");
        _;
    }

    // Modifier for admin roles
    modifier onlyOwner() {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Not an admin role");
        _;
    }

    constructor(DonaswapFlammies _donaswapFlammies) public {
        donaswapFlammies = _donaswapFlammies;
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    }

    /**
     * @notice Mint NFTs from the DonaswapFlammies contract.
     * Users can specify what flammyId they want to mint. Users can claim once.
     * There is a limit on how many are distributed. It requires FLAME balance to be > 0.
     */
    function mintCollectible(
        address _tokenReceiver,
        string calldata _tokenURI,
        uint8 _flammyId
    ) external onlyMinter returns (uint256) {
        uint256 tokenId = donaswapFlammies.mint(_tokenReceiver, _tokenURI, _flammyId);
        return tokenId;
    }

    /**
     * @notice Set up names for flammies.
     * @dev Only the main admins can set it.
     */
    function setFlammyName(uint8 _flammyId, string calldata _flammyName) external onlyOwner {
        donaswapFlammies.setFlammyName(_flammyId, _flammyName);
    }

    /**
     * @dev It transfers the ownership of the NFT contract to a new address.
     * @dev Only the main admins can set it.
     */
    function changeOwnershipNFTContract(address _newOwner) external onlyOwner {
        donaswapFlammies.transferOwnership(_newOwner);
    }
}
