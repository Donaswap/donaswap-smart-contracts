import { ethers, network } from "hardhat";
import config from "../../config";

const currentNetwork = network.name;

const main = async () => {
  console.log("Deploying to network:", currentNetwork);

  const _thresholdUser = "";
  const _endBlock = "";

  const FlammySpecialV2 = await ethers.getContractFactory("FlammySpecialV2");

  const flammySpecialV2 = await FlammySpecialV2.deploy(
    config.FlammyMintingStation[currentNetwork],
    config.FlameToken[currentNetwork],
    config.DonaswapProfile[currentNetwork],
    _thresholdUser,
    _endBlock
  );

  await flammySpecialV2.deployed();
  console.log("FlammySpecialV2 deployed to:", flammySpecialV2.address);
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
