# FlammyFactoryV2

This document explains how the `FlammyFactoryV2` contract works.

`FlammyFactoryV2` inherits from [`Ownable`](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Owanable.sol). Hence, it also inherits from [`Context`](https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/Context.sol).

## 1. Variables

### Public variables

#### Numeric variables (uint256)

- `startBlockNumber`: starting block for minting flammies (can be updated)
- `endBlockNumber`: end block number to get collectibles (can be extended)
- `tokenPrice`: price in FLAME (w/ 18 decimals) to mint a NFT

#### Mappings

- `hasClaimed`: checks whether a user has claimed. If false, he can claim (and pay the price)

### Private variables

#### Numeric variables (uint8)

- `previousNumberFlammyIds`: number of series before (5)
- `numberFlammyIds`: number of total visuals (10)

#### String variables

- `ipfsHash`: IPFS hash for json files with metadata

#### Mappings

- `flammyIdURIs`: maps the flammyId to the respective token URI for the minting function

## 2. Functions

Note: all functions that are not inherited are external.

### Users

- `mintNFT(_tokenId)`: mint a new NFT (with `_tokenId` from 5-9)

## 3. Events

```
event FlammyMint(
    address indexed to,
    uint256 indexed tokenId,
    uint8 indexed flammyId
);
```

It indicates that a new flammy was minted.
