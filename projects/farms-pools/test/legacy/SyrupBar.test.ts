import { expectRevert, time } from "@openzeppelin/test-helpers";
import { artifacts, contract } from "hardhat";
import { assert } from "chai";

const FlameToken = artifacts.require("FlameToken");
const SyrupBar = artifacts.require("SyrupBar");

contract("SyrupBar", ([alice, bob, minter]) => {
  let flame, syrup;

  beforeEach(async () => {
    flame = await FlameToken.new({ from: minter });
    syrup = await SyrupBar.new(flame.address, { from: minter });
  });

  it("mint", async () => {
    await syrup.mint(alice, 1000, { from: minter });
    assert.equal((await syrup.balanceOf(alice)).toString(), "1000");
  });

  it("burn", async () => {
    await time.advanceBlockTo("650");
    await syrup.mint(alice, 1000, { from: minter });
    await syrup.mint(bob, 1000, { from: minter });
    assert.equal((await syrup.totalSupply()).toString(), "2000");
    await syrup.burn(alice, 200, { from: minter });

    assert.equal((await syrup.balanceOf(alice)).toString(), "800");
    assert.equal((await syrup.totalSupply()).toString(), "1800");
  });

  it("safeFlameTransfer", async () => {
    assert.equal((await flame.balanceOf(syrup.address)).toString(), "0");
    await flame.mint(syrup.address, 1000, { from: minter });
    await syrup.safeFlameTransfer(bob, 200, { from: minter });
    assert.equal((await flame.balanceOf(bob)).toString(), "200");
    assert.equal((await flame.balanceOf(syrup.address)).toString(), "800");
    await syrup.safeFlameTransfer(bob, 2000, { from: minter });
    assert.equal((await flame.balanceOf(bob)).toString(), "1000");
  });
});
