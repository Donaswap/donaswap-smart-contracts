import { ethers, network } from "hardhat";
import { parseEther } from "ethers/lib/utils";
import config from "../../config";

const currentNetwork = network.name;

const main = async () => {
  console.log("Deploying to network:", currentNetwork);

  const _numberFlameToRegister = parseEther("1"); // 1 FLAME
  const _numberFlameToReactivate = parseEther("2"); // 2 FLAME
  const _numberFlameToUpdate = parseEther("2"); // 2 FLAME

  const DonaswapProfile = await ethers.getContractFactory("DonaswapProfile");

  const donaswapProfile = await DonaswapProfile.deploy(
    config.FlameToken[currentNetwork],
    _numberFlameToReactivate,
    _numberFlameToRegister,
    _numberFlameToUpdate
  );

  console.log("DonaswapProfile deployed to:", donaswapProfile.address);

  await donaswapProfile.addTeam("Syrup Storm", "ipfs://QmamkDch4WBYGbchd6NV7MzPvG1NgWqWHNnYogdzreNtBn/syrup-storm.json");
  await donaswapProfile.addTeam(
    "Fearsome Flippers",
    "ipfs://QmamkDch4WBYGbchd6NV7MzPvG1NgWqWHNnYogdzreNtBn/fearsome-flippers.json"
  );
  await donaswapProfile.addTeam(
    "Chaotic Flamers",
    "ipfs://QmamkDch4WBYGbchd6NV7MzPvG1NgWqWHNnYogdzreNtBn/chaotic-flamers.json"
  );
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
