import { ethers, network } from "hardhat";
import { parseEther } from "ethers/lib/utils";
import config from "../../config";

const currentNetwork = network.name;

const main = async () => {
  console.log("Deploying to network:", currentNetwork);

  const _tokenPrice = parseEther("4");
  const _ipfsHash = "";
  const _startBlockTime = "";

  const FlammyMintingStation = await ethers.getContractFactory("FlammyMintingStation");

  const flammyMintingStation = await FlammyMintingStation.deploy(config.DonaswapFlammies[currentNetwork]);

  await flammyMintingStation.deployed();
  console.log("FlammyMintingStation deployed to:", flammyMintingStation.address);

  const FlammyFactoryV3 = await ethers.getContractFactory("FlammyFactoryV3");

  const flammyFactory = await FlammyFactoryV3.deploy(
    config.FlammyFactoryV2[currentNetwork],
    flammyMintingStation.address,
    config.FlameToken[currentNetwork],
    _tokenPrice,
    _ipfsHash,
    _startBlockTime
  );

  await flammyFactory.deployed();
  console.log("FlammyFactoryV3 deployed to:", flammyFactory.address);

  await flammyFactory.setFlammyJson("sleepy.json", "dollop.json", "twinkle.json", "churro.json", "sunny.json");

  const MINTER_ROLE = await flammyMintingStation.MINTER_ROLE();
  await flammyMintingStation.grantRole(MINTER_ROLE, flammyFactory);
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
