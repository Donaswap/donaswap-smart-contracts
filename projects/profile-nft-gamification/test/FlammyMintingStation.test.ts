import { assert } from "chai";
import { BN, expectEvent, expectRevert, time } from "@openzeppelin/test-helpers";
import { artifacts, contract } from "hardhat";
import { parseEther } from "ethers/lib/utils";

const MockBEP20 = artifacts.require("./utils/MockBEP20.sol");
const MockFlammies = artifacts.require("./utils/MockFlammies.sol");
const AnniversaryAchievement = artifacts.require("./AnniversaryAchievement.sol");
const FlammyFactoryV2 = artifacts.require("./old/FlammyFactoryV2.sol");
const FlammyFactoryV3 = artifacts.require("./FlammyFactoryV3.sol");
const FlammySpecialV1 = artifacts.require("./FlammySpecialV1.sol");
const FlammySpecialV2 = artifacts.require("./FlammySpecialV2.sol");
const FlammyFlameVault = artifacts.require("./FlammySpecialFlameVault.sol");
const FlammyPrediction = artifacts.require("./FlammySpecialPrediction.sol");
const FlammyMintingStation = artifacts.require("./FlammyMintingStation.sol");
const DonaswapFlammies = artifacts.require("./DonaswapFlammies.sol");
const DonaswapProfile = artifacts.require("./DonaswapProfile.sol");
const FlammySpecialLottery = artifacts.require("./FlammySpecialLottery.sol");
const FlammySpecialAdmin = artifacts.require("./FlammySpecialAdmin.sol");

// FLAME VAULT
const FlameVault = artifacts.require("donaswap-flame-vault/contracts/FlameVault.sol");
const FlameToken = artifacts.require("./test/FlameToken.sol");
const SyrupBar = artifacts.require("./test/SyrupBar.sol");
const MasterChef = artifacts.require("./test/MasterChef.sol");

// PREDICTION
const BnbPricePrediction = artifacts.require("predictions/contracts/BnbPricePrediction.sol");
const MockAggregatorV3 = artifacts.require("test/MockAggregatorV3.sol");

// LOTTERY
const MockDonaswapLottery = artifacts.require("test/MockDonaswapLottery.sol");

contract("FlammyFactoryV3 and above", ([alice, bob, carol, david, eve, frank]) => {
  let anniversaryAchievement;
  let flammyFactoryV2;
  let flammyFactoryV3;
  let flammyMintingStation;
  let flammySpecialV1;
  let flammySpecialV2;
  let mockFLAME;
  let donaswapFlammies;
  let donaswapProfile;
  let mockFlammies;

  let vault;
  let masterchef;
  let flame;
  let syrup;
  let rewardsStartBlock;
  let flammyFlameVault;

  let prediction;
  let mockAggregatorV3;
  let flammyPrediction;

  let donaswapLottery;
  let flammySpecialLottery;

  let flammySpecialAdmin;

  let result;
  let currentBlockNumber;
  let currentTimestamp;
  let DEFAULT_ADMIN_ROLE;
  let MINTER_ROLE;
  let POINT_ROLE;

  const _tokenPrice = parseEther("1"); // 1 FLAME
  const _ipfsHash = "test/";
  const _endBlockNumberV2 = "700";
  const _startBlockNumberV2 = "1";
  const _startBlockNumberV3 = "1000";

  // Donaswap Profile related.
  const _numberPoints = 100;
  const _campaignId = "123456789";

  before(async () => {
    mockFLAME = await MockBEP20.new("Donaswap Mock Token", "FLAME", 0, {
      from: alice,
    });

    donaswapFlammies = await DonaswapFlammies.new("ipfs://", { from: alice });

    // Deploy V2
    flammyFactoryV2 = await FlammyFactoryV2.new(
      donaswapFlammies.address,
      mockFLAME.address,
      _tokenPrice,
      _ipfsHash,
      _startBlockNumberV2,
      _endBlockNumberV2,
      { from: alice }
    );

    // Transfer ownership to V2
    await donaswapFlammies.transferOwnership(flammyFactoryV2.address, {
      from: alice,
    });

    await flammyFactoryV2.setFlammyNames("MyFlammy5", "MyFlammy6", "MyFlammy7", "MyFlammy8", "MyFlammy9", {
      from: alice,
    });

    await flammyFactoryV2.setFlammyJson("test5.json", "test6.json", "test7.json", "test8.json", "test9.json", {
      from: alice,
    });

    await mockFLAME.mintTokens("1000000000000000000", { from: alice });

    await mockFLAME.approve(flammyFactoryV2.address, "1000000000000000000", {
      from: alice,
    });

    await flammyFactoryV2.mintNFT("6", { from: alice });

    flammyMintingStation = await FlammyMintingStation.new(donaswapFlammies.address);

    flammyFactoryV3 = await FlammyFactoryV3.new(
      flammyFactoryV2.address,
      flammyMintingStation.address,
      mockFLAME.address,
      _tokenPrice,
      _ipfsHash,
      _startBlockNumberV3,
      { from: alice }
    );

    await flammyFactoryV2.changeOwnershipNFTContract(flammyMintingStation.address, {
      from: alice,
    });

    DEFAULT_ADMIN_ROLE = await flammyMintingStation.DEFAULT_ADMIN_ROLE();
    MINTER_ROLE = await flammyMintingStation.MINTER_ROLE();

    // FLAME VAULT
    rewardsStartBlock = (await time.latestBlock()).toNumber();
    flame = await FlameToken.new({ from: alice });
    syrup = await SyrupBar.new(flame.address, { from: alice });
    masterchef = await MasterChef.new(flame.address, syrup.address, alice, parseEther("1"), rewardsStartBlock, {
      from: alice,
    }); // 1 flame per block, starts at +100 block of each test
    vault = await FlameVault.new(flame.address, syrup.address, masterchef.address, bob, carol, { from: alice });

    await flame.mint(alice, parseEther("100"), { from: alice });
    await flame.mint(bob, parseEther("100"), { from: alice });
    await flame.mint(carol, parseEther("100"), { from: alice });
    await flame.mint(frank, parseEther("100"), { from: alice });
    await flame.approve(vault.address, parseEther("1000"), { from: alice });
    await flame.approve(vault.address, parseEther("1000"), { from: bob });
    await flame.approve(vault.address, parseEther("1000"), { from: carol });
    await flame.approve(vault.address, parseEther("1000"), { from: frank });
    await flame.transferOwnership(masterchef.address, { from: alice });
    await syrup.transferOwnership(masterchef.address, { from: alice });

    // PREDICTION
    mockAggregatorV3 = await MockAggregatorV3.new("8", "10000000000", { from: alice });
    prediction = await BnbPricePrediction.new(mockAggregatorV3.address, alice, alice, 20, parseEther("1"), 1, 1, {
      from: alice,
    });

    // LOTTERY
    mockFlammies = await MockFlammies.new({ from: alice });
    donaswapLottery = await MockDonaswapLottery.new(flame.address);
  });

  describe("All new contracts are deployed correctly", async () => {
    it("Symbol and names are correct", async () => {
      assert.equal(await donaswapFlammies.symbol(), "PB");
      assert.equal(await donaswapFlammies.name(), "Donaswap Flammies");
    });

    it("Owners & roles are ok", async () => {
      assert.equal(await donaswapFlammies.owner(), flammyMintingStation.address);
      assert.equal(await flammyFactoryV3.owner(), alice);
      assert.equal(await flammyFactoryV3.canMint(alice), false);
      assert.equal(await flammyFactoryV3.canMint(bob), true);
      assert.equal(await flammyMintingStation.getRoleMemberCount(DEFAULT_ADMIN_ROLE), "1");
      assert.equal(await flammyMintingStation.getRoleMemberCount(MINTER_ROLE), "0");
    });
  });

  describe("FlammyMintingStation works", async () => {
    it("Only approved admin can mint", async () => {
      await expectRevert(
        flammyMintingStation.mintCollectible(alice, "aliceFlammy.json", "10", {
          from: alice,
        }),
        "Not a minting role"
      );
    });

    it("Security checks are executed", async () => {
      await expectRevert(flammyFactoryV3.mintNFT(1, { from: bob }), "too early");

      await time.advanceBlockTo(1001);

      await expectRevert(flammyFactoryV3.mintNFT(4, { from: bob }), "flammyId too low");

      await expectRevert(flammyFactoryV3.mintNFT(10, { from: bob }), "flammyId too high");

      await expectRevert(flammyFactoryV3.mintNFT(5, { from: bob }), "BEP20: transfer amount exceeds balance");

      await mockFLAME.mintTokens("100000000000000000000", { from: bob });

      result = await mockFLAME.approve(flammyFactoryV3.address, "100000000000000000000", { from: bob });

      expectEvent(result, "Approval");

      assert.equal(await flammyFactoryV3.canMint(bob), true);

      await expectRevert(flammyFactoryV3.mintNFT(9, { from: bob }), "Not a minting role");
    });

    it("Alice adds minting role for flammyMintingStation", async () => {
      result = await flammyMintingStation.grantRole(MINTER_ROLE, flammyFactoryV3.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: MINTER_ROLE,
        account: flammyFactoryV3.address,
        sender: alice,
      });

      await flammyMintingStation.setFlammyName(5, "MyFlammy5", { from: alice });
      await flammyMintingStation.setFlammyName(6, "MyFlammy6", { from: alice });
      await flammyMintingStation.setFlammyName(7, "MyFlammy7", { from: alice });
      await flammyMintingStation.setFlammyName(8, "MyFlammy8", { from: alice });
      await flammyMintingStation.setFlammyName(9, "MyFlammy9", { from: alice });

      result = await flammyFactoryV3.setFlammyJson("test5.json", "test6.json", "test7.json", "test8.json", "test9.json", {
        from: alice,
      });

      assert.equal(await flammyFactoryV3.hasClaimed(bob), false);
      assert.equal(await flammyFactoryV3.canMint(bob), true);

      result = await flammyFactoryV3.mintNFT(9, { from: bob });

      expectEvent(result, "FlammyMint", {
        to: bob,
        tokenId: "1",
        flammyId: "9",
      });

      assert.equal(await flammyFactoryV3.canMint(bob), false);
      assert.equal(await flammyFactoryV3.hasClaimed(bob), true);

      assert.equal(await donaswapFlammies.totalSupply(), "2");
      assert.equal(await donaswapFlammies.tokenURI(1), "ipfs://test/test9.json");
    });

    it("Bob cannot claim twice", async () => {
      await expectRevert(flammyFactoryV3.mintNFT(9, { from: bob }), "Has claimed");
    });

    it("Alice changes the block number", async () => {
      await expectRevert(flammyFactoryV3.setStartBlockNumber("10", { from: alice }), "too short");

      await flammyFactoryV3.setStartBlockNumber("1200", { from: alice });
      assert.equal(await flammyFactoryV3.startBlockNumber(), "1200");

      await expectRevert(flammyFactoryV3.mintNFT(6, { from: carol }), "too early");
    });

    it("Alice updates token price to 5 BUSD", async () => {
      await time.advanceBlockTo(1201);

      await flammyFactoryV3.updateTokenPrice("5000000000000000000", {
        from: alice,
      });
      assert.equal(await flammyFactoryV3.tokenPrice(), "5000000000000000000");
    });

    it("Carol mints a NFT for 5 BUSD", async () => {
      await mockFLAME.mintTokens("100000000000000000000", { from: carol });

      await mockFLAME.approve(flammyFactoryV3.address, "100000000000000000000", {
        from: carol,
      });

      result = await flammyFactoryV3.mintNFT(6, { from: carol });

      expectEvent(result, "FlammyMint", {
        to: carol,
        tokenId: "2",
        flammyId: "6",
      });

      result = await mockFLAME.balanceOf(carol);

      assert.equal(result.toString(), "95000000000000000000"); // 95 FLAME

      assert.equal(await donaswapFlammies.totalSupply(), "3");
      assert.equal(await donaswapFlammies.tokenURI(2), "ipfs://test/test6.json");
    });

    it("Alice claims FLAME fees", async () => {
      await flammyFactoryV3.claimFee("6000000000000000000", { from: alice });

      result = await mockFLAME.balanceOf(flammyFactoryV3.address);
      assert.equal(result.toString(), "0");

      result = await mockFLAME.balanceOf(alice);
      assert.equal(result.toString(), "6000000000000000000");
    });
  });

  describe("Admin and ownership tests", async () => {
    it("Frank cannot access functions as he is not owner of FlammyFactoryV3", async () => {
      await expectRevert(
        flammyFactoryV3.transferOwnership(frank, {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV3.claimFee("1", {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV3.setStartBlockNumber(1, {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV3.updateTokenPrice(1, {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV3.setFlammyJson("a1", "a2", "a3", "a4", "a5", {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );
    });

    it("Frank cannot access functions as he is not main admin of FlammyMintingFarm", async () => {
      await expectRevert(
        flammyMintingStation.setFlammyName(1, "Poopies", {
          from: frank,
        }),
        "Not an admin role"
      );

      await expectRevert(
        flammyMintingStation.changeOwnershipNFTContract(frank, {
          from: frank,
        }),
        "Not an admin role"
      );
    });

    it("Alice can transfer ownership of DonaswapFlammies", async () => {
      result = await flammyMintingStation.changeOwnershipNFTContract(alice, {
        from: alice,
      });

      expectEvent.inTransaction(result.receipt.transactionHash, donaswapFlammies, "OwnershipTransferred", {
        previousOwner: flammyMintingStation.address,
        newOwner: alice,
      });

      assert.equal(await donaswapFlammies.owner(), alice);
    });

    it("Alice cannot mint anymore since she minted in V2", async () => {
      assert.equal(await flammyFactoryV3.canMint(alice), false);
      await expectRevert(flammyFactoryV3.mintNFT(9, { from: alice }), "Has claimed in v2");
    });

    it("David cannot mint anymore", async () => {
      await mockFLAME.mintTokens("100000000000000000000", { from: david });

      await mockFLAME.approve(flammyFactoryV3.address, "100000000000000000000", { from: david });

      await expectRevert(flammyFactoryV3.mintNFT(9, { from: david }), "Ownable: caller is not the owner");
    });

    it("Alice transfers ownership back to FlammyMintingStation", async () => {
      result = await donaswapFlammies.transferOwnership(flammyMintingStation.address, {
        from: alice,
      });

      expectEvent(result, "OwnershipTransferred", {
        previousOwner: alice,
        newOwner: flammyMintingStation.address,
      });

      assert.equal(await donaswapFlammies.owner(), flammyMintingStation.address);
    });
  });
  describe("FlammySpecialV1", async () => {
    it("Alice deploys DonaswapProfile and Bob creates a profile", async () => {
      const _numberFlameToReactivate = parseEther("2"); // 2 FLAME
      const _numberFlameToRegister = parseEther("1"); // 1 FLAME
      const _numberFlameToUpdate = parseEther("2"); // 2 FLAME

      donaswapProfile = await DonaswapProfile.new(
        mockFLAME.address,
        _numberFlameToReactivate,
        _numberFlameToRegister,
        _numberFlameToUpdate,
        { from: alice }
      );

      // Donaswap Profile roles.
      POINT_ROLE = await donaswapProfile.POINT_ROLE();

      await donaswapProfile.addTeam("The Testers", "ipfs://hash/team1.json", {
        from: alice,
      });

      await donaswapProfile.addNftAddress(donaswapFlammies.address, {
        from: alice,
      });

      await donaswapFlammies.approve(donaswapProfile.address, "1", {
        from: bob,
      });

      await mockFLAME.approve(donaswapProfile.address, parseEther("500"), {
        from: bob,
      });

      await donaswapProfile.createProfile("1", donaswapFlammies.address, "1", {
        from: bob,
      });
    });

    it("Alice deploys FlammySpecialV1", async () => {
      flammySpecialV1 = await FlammySpecialV1.new(
        flammyMintingStation.address,
        mockFLAME.address,
        donaswapProfile.address,
        "10",
        { from: alice }
      );

      assert.equal(await flammySpecialV1.maxViewLength(), "10");
      assert.equal(await flammySpecialV1.owner(), alice);
    });

    it("It is not possible to claim if NFT doesn't exist", async () => {
      await expectRevert(flammySpecialV1.mintNFT(9, { from: bob }), "ERR_ID_LOW");

      await expectRevert(flammySpecialV1.mintNFT(10, { from: bob }), "ERR_ID_INVALID");
    });

    it("Alice adds a new flammyId", async () => {
      await flammyMintingStation.setFlammyName("10", "Hiccup", { from: alice });
      assert.equal(await donaswapFlammies.getFlammyName("10"), "Hiccup");
      result = await flammySpecialV1.addFlammy(10, "hash/hiccup.json", 2, 0, {
        from: alice,
      });

      expectEvent(result, "FlammyAdd", {
        flammyId: "10",
        thresholdUser: "2",
        costFlame: "0",
      });

      result = await flammySpecialV1.flammyCharacteristics("10");

      assert.equal(result[0], "hash/hiccup.json");
      assert.equal(result[1], "2");
      assert.equal(result[2], "0");
      assert.equal(result[3], true);
      assert.equal(result[4], true);

      result = await flammySpecialV1.flammyCharacteristics("11");

      assert.equal(result[0], "");
      assert.equal(result[1], "0");
      assert.equal(result[2], "0");
      assert.equal(result[3], false);
      assert.equal(result[4], false);
    });

    it("Bob cannot mint until it is supported as a minter", async () => {
      await expectRevert(flammySpecialV1.mintNFT(10, { from: bob }), "Not a minting role");

      result = await flammyMintingStation.grantRole(MINTER_ROLE, flammySpecialV1.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: MINTER_ROLE,
        account: flammySpecialV1.address,
        sender: alice,
      });
    });

    it("Bob can mint", async () => {
      assert.equal(await flammySpecialV1.canClaimSingle(bob, "10"), true);
      assert.equal(await flammySpecialV1.canClaimSingle(bob, "11"), false);
      assert.equal(await flammySpecialV1.canClaimSingle(bob, "9"), false);

      result = await flammySpecialV1.mintNFT(10, { from: bob });

      expectEvent(result, "FlammyMint", {
        to: bob,
        tokenId: "3",
        flammyId: "10",
      });

      assert.equal(await donaswapFlammies.tokenURI("3"), "ipfs://hash/hiccup.json");
      assert.equal(await donaswapFlammies.getFlammyNameOfTokenId("3"), "Hiccup");
      assert.equal(await flammySpecialV1.canClaimSingle(bob, "10"), false);
    });

    it("Bob cannot mint twice", async () => {
      await expectRevert(flammySpecialV1.mintNFT(10, { from: bob }), "ERR_HAS_CLAIMED");
    });

    it("Carol cannot mint since has no profile", async () => {
      await expectRevert(flammySpecialV1.mintNFT(10, { from: carol }), "Not registered");
    });

    it("Carol creates a profile but is below threshold", async () => {
      await donaswapFlammies.approve(donaswapProfile.address, "2", {
        from: carol,
      });

      await mockFLAME.approve(donaswapProfile.address, parseEther("500"), {
        from: carol,
      });

      assert.equal(await flammySpecialV1.canClaimSingle(carol, "10"), false);

      await donaswapProfile.createProfile("1", donaswapFlammies.address, "2", {
        from: carol,
      });

      assert.equal(await flammySpecialV1.canClaimSingle(carol, "10"), false);

      await expectRevert(flammySpecialV1.mintNFT(10, { from: carol }), "ERR_USER_NOT_ELIGIBLE");
    });

    it("Admin changes threshold for Carol to be eligible", async () => {
      await expectRevert(flammySpecialV1.updateFlammy("11", "3", "0", true, { from: alice }), "ERR_NOT_CREATED");

      await flammySpecialV1.updateFlammy("10", "5", "0", true, {
        from: alice,
      });

      assert.equal(await flammySpecialV1.canClaimSingle(carol, "10"), true);
    });

    it("Carol pauses her profile and cannot claim flammy", async () => {
      await donaswapProfile.pauseProfile({ from: carol });

      assert.equal(await flammySpecialV1.canClaimSingle(carol, "10"), false);

      assert.equal(await flammySpecialV1.canClaimMultiple(carol, ["10", "11"]), "");

      await expectRevert(flammySpecialV1.mintNFT(10, { from: carol }), "ERR_USER_NOT_ACTIVE");
    });

    it("Alice makes flammyId=10 inactive while Carol re-activates her profile", async () => {
      await donaswapFlammies.approve(donaswapProfile.address, "2", {
        from: carol,
      });

      await donaswapProfile.reactivateProfile(donaswapFlammies.address, "2", {
        from: carol,
      });

      result = await flammySpecialV1.updateFlammy("10", "5", "0", false, {
        from: alice,
      });

      expectEvent(result, "FlammyChange", {
        flammyId: "10",
        thresholdUser: "5",
        costFlame: "0",
        isActive: false,
      });

      result = await flammySpecialV1.flammyCharacteristics("10");

      assert.equal(result[0], "hash/hiccup.json");
      assert.equal(result[1], "5");
      assert.equal(result[2], "0");
      assert.equal(result[3], false);

      assert.equal(await flammySpecialV1.canClaimSingle(carol, "10"), false);

      await expectRevert(flammySpecialV1.mintNFT(10, { from: carol }), "ERR_ID_INVALID");
    });

    it("Alice makes flammy active again but adds a price", async () => {
      // Alice adds a price of 88 wei of FLAME
      await flammySpecialV1.updateFlammy("10", "5", "88", true, {
        from: alice,
      });

      result = await flammySpecialV1.flammyCharacteristics("10");

      assert.equal(result[0], "hash/hiccup.json");
      assert.equal(result[1], "5");
      assert.equal(result[2], "88");
      assert.equal(result[3], true);

      assert.equal(await flammySpecialV1.canClaimSingle(carol, "10"), true);

      await expectRevert(flammySpecialV1.mintNFT(10, { from: carol }), "BEP20: transfer amount exceeds allowance");
    });

    it("Carol approves the contract to spend FLAME and mints", async () => {
      await mockFLAME.approve(flammySpecialV1.address, "88", {
        from: carol,
      });

      await flammySpecialV1.mintNFT(10, { from: carol });

      assert.equal(await donaswapFlammies.tokenURI("4"), "ipfs://hash/hiccup.json");
      assert.equal(await donaswapFlammies.getFlammyNameOfTokenId("4"), "Hiccup");
      assert.equal(await flammySpecialV1.canClaimSingle(carol, "10"), false);
      assert.equal(await mockFLAME.balanceOf(flammySpecialV1.address), "88");
    });

    it("Alice claims the money", async () => {
      await flammySpecialV1.claimFee("88", { from: alice });
      assert.equal(await mockFLAME.balanceOf(flammySpecialV1.address), "0");
    });

    it("Alice adds a second new flammy", async () => {
      await flammyMintingStation.setFlammyName("11", "Bullish", {
        from: alice,
      });

      assert.equal(await donaswapFlammies.getFlammyName("11"), "Bullish");

      // It already exists
      await expectRevert(
        flammySpecialV1.addFlammy(10, "hash2/bullish.json", 5, "8888", {
          from: alice,
        }),
        "ERR_CREATED"
      );

      // It is too low
      await expectRevert(
        flammySpecialV1.addFlammy(9, "hash2/bullish.json", 5, "8888", {
          from: alice,
        }),
        "ERR_ID_LOW_2"
      );

      result = await flammySpecialV1.addFlammy(11, "hash2/bullish.json", 5, "8888", {
        from: alice,
      });

      expectEvent(result, "FlammyAdd", {
        flammyId: "11",
        thresholdUser: "5",
        costFlame: "8888",
      });

      result = await flammySpecialV1.flammyCharacteristics("11");

      assert.equal(result[0], "hash2/bullish.json");
      assert.equal(result[1], "5");
      assert.equal(result[2], "8888");
      assert.equal(result[3], true);
    });

    it("Tests for canClaimMultiple and length", async () => {
      result = await flammySpecialV1.canClaimMultiple(bob, ["11", "10", "3"]);

      assert.sameOrderedMembers(result, [true, false, false]);

      assert.equal(await flammySpecialV1.canClaimSingle(frank, "2"), false);

      assert.equal(await flammySpecialV1.canClaimMultiple(frank, ["2", "10"]), false);

      assert.equal(await flammySpecialV1.maxViewLength(), "10");

      await flammySpecialV1.updateMaxViewLength("2", { from: alice });
      assert.equal(await flammySpecialV1.maxViewLength(), "2");

      await expectRevert(flammySpecialV1.canClaimMultiple(bob, ["11", "10", "3"]), "ERR_LENGTH_VIEW");
    });

    it("Tests for ownership", async () => {
      await expectRevert(
        flammySpecialV1.addFlammy(9, "hash2/bullish.json", 5, "8888", {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammySpecialV1.updateFlammy("11", "3", "0", true, { from: bob }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(flammySpecialV1.claimFee("88", { from: bob }), "Ownable: caller is not the owner");
    });
  });
  describe("FlammySpecialV2", async () => {
    it("Contract is deployed", async () => {
      currentBlockNumber = await time.latestBlock();
      const _endBlockNumberS2 = currentBlockNumber.add(new BN(40));
      const _thresholdUserS2 = "3";

      // Deploy V2
      flammySpecialV2 = await FlammySpecialV2.new(
        flammyMintingStation.address,
        mockFLAME.address,
        donaswapProfile.address,
        _thresholdUserS2,
        _endBlockNumberS2,
        { from: alice }
      );
      // Grant minting role to the new contract
      result = await flammyMintingStation.grantRole(MINTER_ROLE, flammySpecialV2.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: MINTER_ROLE,
        account: flammySpecialV2.address,
        sender: alice,
      });
    });

    it("Cannot mint if flammies were not set", async () => {
      assert.equal(String(await flammySpecialV2.teamIdToFlammyId("1")), "0");
      await expectRevert(flammySpecialV2.mintNFT({ from: bob }), "NOT_VALID");
    });

    it("Owner adds flammies", async () => {
      result = await flammySpecialV2.addFlammy("12", "1", "team1Flammy.json", {
        from: alice,
      });

      expectEvent(result, "FlammyAdd", { flammyId: "12", teamId: "1" });

      assert.equal(String(await flammySpecialV2.teamIdToFlammyId("1")), "12");

      result = await flammySpecialV2.addFlammy("13", "2", "team2Flammy.json", {
        from: alice,
      });

      expectEvent(result, "FlammyAdd", { flammyId: "13", teamId: "2" });

      assert.equal(String(await flammySpecialV2.teamIdToFlammyId("2")), "13");

      result = await flammySpecialV2.addFlammy("14", "3", "team2Flammy.json", {
        from: alice,
      });

      expectEvent(result, "FlammyAdd", { flammyId: "14", teamId: "3" });

      assert.equal(String(await flammySpecialV2.teamIdToFlammyId("3")), "14");
    });

    it("Bob can mint", async () => {
      assert.equal(await flammySpecialV2.canClaim(bob), true);

      result = await flammySpecialV2.mintNFT({ from: bob });

      expectEvent(result, "FlammyMint", {
        to: bob,
        tokenId: "5",
        flammyId: "12",
      });

      assert.equal(await flammySpecialV2.canClaim(bob), false);
    });

    it("Bob cannot mint twice", async () => {
      await expectRevert(flammySpecialV2.mintNFT({ from: bob }), "ERR_HAS_CLAIMED");
    });

    it("Frank cannot mint as he doesn't have profile", async () => {
      assert.equal(await flammySpecialV2.canClaim(frank), false);

      await expectRevert(flammySpecialV2.mintNFT({ from: frank }), "Not registered");
    });

    it("Carol cannot mint without an active profile", async () => {
      assert.equal(await flammySpecialV2.canClaim(carol), true);

      await donaswapProfile.pauseProfile({ from: carol });
      await expectRevert(flammySpecialV2.mintNFT({ from: carol }), "ERR_USER_NOT_ACTIVE");

      assert.equal(await flammySpecialV2.canClaim(carol), false);
    });

    it("Carol can mint with an active profile", async () => {
      await donaswapFlammies.approve(donaswapProfile.address, "2", {
        from: carol,
      });

      await donaswapProfile.reactivateProfile(donaswapFlammies.address, "2", {
        from: carol,
      });

      assert.equal(await flammySpecialV2.canClaim(carol), true);

      result = await flammySpecialV2.mintNFT({ from: carol });

      expectEvent(result, "FlammyMint", {
        to: carol,
        tokenId: "6",
        flammyId: "12",
      });

      assert.equal(await flammySpecialV2.canClaim(carol), false);
    });

    it("David cannot mint as end block has passed", async () => {
      await time.advanceBlockTo(currentBlockNumber.add(new BN(40)));
      await expectRevert(flammySpecialV2.mintNFT({ from: david }), "TOO_LATE");
    });

    it("Owner changes end block", async () => {
      const newEndBlock = currentBlockNumber.add(new BN(60));
      result = await flammySpecialV2.changeEndBlock(newEndBlock, {
        from: alice,
      });
      expectEvent(result, "NewEndBlock", {
        endBlock: String(newEndBlock),
      });
    });

    it("David cannot mint as he is outside of eligible threshold", async () => {
      // David mints FLAME/NFT/creates a profile (userId = 3)
      await mockFLAME.mintTokens(parseEther("100"), { from: david });
      assert.equal(await flammySpecialV2.canClaim(david), false);

      await mockFLAME.approve(flammyFactoryV3.address, parseEther("100"), {
        from: david,
      });

      result = await flammyFactoryV3.mintNFT(9, { from: david });

      expectEvent(result, "FlammyMint", {
        to: david,
        tokenId: "7",
        flammyId: "9",
      });

      await donaswapFlammies.approve(donaswapProfile.address, "7", {
        from: david,
      });

      await mockFLAME.approve(donaswapProfile.address, parseEther("100"), {
        from: david,
      });

      await donaswapProfile.createProfile("1", donaswapFlammies.address, "7", {
        from: david,
      });

      assert.equal(await flammySpecialV2.canClaim(david), false);

      await expectRevert(flammySpecialV2.mintNFT({ from: david }), "ERR_USER_NOT_ELIGIBLE");
    });

    it("Only owner can call ownable functions changes end block", async () => {
      await expectRevert(
        flammySpecialV2.changeEndBlock(105, {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );
    });

    it("Owner cannot add a flammyId if below threshold", async () => {
      await expectRevert(
        flammySpecialV2.addFlammy("11", "3", "impossible.json", {
          from: alice,
        }),
        "ERR_ID_LOW_2"
      );
    });
  });
  describe("FlammySpecialFlameVault", async () => {
    it("Contract is deployed", async () => {
      currentBlockNumber = await time.latestBlock();
      const _endBlockNumberS3 = currentBlockNumber.add(new BN(75));
      currentTimestamp = await time.latest();
      const _thresholdTimestampS3 = currentTimestamp.add(new BN(10));

      // Deploy FlameVault
      flammyFlameVault = await FlammyFlameVault.new(
        vault.address,
        flammyMintingStation.address,
        donaswapProfile.address,
        _endBlockNumberS3,
        _thresholdTimestampS3,
        _numberPoints,
        _campaignId,
        "kekFlammy.json",
        { from: alice }
      );

      // Grants point role to the new contract
      result = await donaswapProfile.grantRole(POINT_ROLE, flammyFlameVault.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: POINT_ROLE,
        account: flammyFlameVault.address,
        sender: alice,
      });

      // Grant minting role to the new contract
      result = await flammyMintingStation.grantRole(MINTER_ROLE, flammyFlameVault.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: MINTER_ROLE,
        account: flammyFlameVault.address,
        sender: alice,
      });
    });

    it("Bob can mint", async () => {
      // Participate in vault
      await vault.deposit(parseEther("10"), { from: bob });

      assert.equal(await flammyFlameVault.canClaim(bob), true);

      result = await flammyFlameVault.mintNFT({ from: bob });

      expectEvent(result, "FlammyMint", {
        to: bob,
        tokenId: "8",
        flammyId: "16",
      });

      expectEvent.inTransaction(result.receipt.transactionHash, donaswapProfile, "UserPointIncrease", {
        userAddress: bob,
        numberPoints: String(_numberPoints),
        campaignId: String(_campaignId),
      });

      assert.equal(await flammyFlameVault.canClaim(bob), false);
    });

    it("Bob cannot mint twice", async () => {
      await expectRevert(flammyFlameVault.mintNFT({ from: bob }), "ERR_HAS_CLAIMED");
    });

    it("Frank cannot mint as he doesn't have profile", async () => {
      assert.equal(await flammyFlameVault.canClaim(frank), false);

      await expectRevert(flammyFlameVault.mintNFT({ from: frank }), "Not registered");
    });

    it("Carol cannot mint without an active profile", async () => {
      // Participate in vault
      await vault.deposit(parseEther("10"), { from: carol });

      assert.equal(await flammyFlameVault.canClaim(carol), true);

      await donaswapProfile.pauseProfile({ from: carol });
      await expectRevert(flammyFlameVault.mintNFT({ from: carol }), "ERR_USER_NOT_ACTIVE");

      assert.equal(await flammyFlameVault.canClaim(carol), false);
    });

    it("Carol can mint with an active profile", async () => {
      await donaswapFlammies.approve(donaswapProfile.address, "2", {
        from: carol,
      });

      await donaswapProfile.reactivateProfile(donaswapFlammies.address, "2", {
        from: carol,
      });

      assert.equal(await flammyFlameVault.canClaim(carol), true);

      result = await flammyFlameVault.mintNFT({ from: carol });

      expectEvent(result, "FlammyMint", {
        to: carol,
        tokenId: "9",
        flammyId: "16",
      });

      expectEvent.inTransaction(result.receipt.transactionHash, donaswapProfile, "UserPointIncrease", {
        userAddress: carol,
        numberPoints: String(_numberPoints),
        campaignId: String(_campaignId),
      });

      assert.equal(await flammyFlameVault.canClaim(carol), false);
    });

    it("Frank cannot mint as he did not participate in flame vault", async () => {
      // Frank mints FLAME/NFT/creates a profile (userId = 4)
      await mockFLAME.mintTokens(parseEther("100"), { from: frank });

      assert.equal(await flammyFlameVault.canClaim(frank), false);

      await mockFLAME.approve(flammyFactoryV3.address, parseEther("100"), {
        from: frank,
      });

      result = await flammyFactoryV3.mintNFT(9, { from: frank });

      expectEvent(result, "FlammyMint", {
        to: frank,
        tokenId: "10",
        flammyId: "9",
      });

      await donaswapFlammies.approve(donaswapProfile.address, "10", {
        from: frank,
      });

      await mockFLAME.approve(donaswapProfile.address, parseEther("100"), {
        from: frank,
      });

      await donaswapProfile.createProfile("1", donaswapFlammies.address, "10", {
        from: frank,
      });

      assert.equal(await flammyFlameVault.canClaim(frank), false);

      await expectRevert(flammyFlameVault.mintNFT({ from: frank }), "ERR_USER_NOT_ELIGIBLE");
    });

    it("Frank cannot mint as he participated too late in the vault", async () => {
      // Advance blocks in order to go out of range
      await time.increaseTo(currentTimestamp.add(new BN(5000000000)));

      // Participate in vault
      await vault.deposit(parseEther("10"), { from: frank });

      assert.equal(await flammyFlameVault.canClaim(frank), false);

      await expectRevert(flammyFlameVault.mintNFT({ from: frank }), "ERR_USER_NOT_ELIGIBLE");
    });

    it("David cannot mint as end block has passed", async () => {
      await time.advanceBlockTo(currentBlockNumber.add(new BN(80)));
      await expectRevert(flammyFlameVault.mintNFT({ from: david }), "TOO_LATE");
    });

    it("Owner changes end block", async () => {
      const newEndBlock = currentBlockNumber.add(new BN(25));
      result = await flammyFlameVault.changeEndBlock(newEndBlock, {
        from: alice,
      });
      expectEvent(result, "NewEndBlock", {
        endBlock: String(newEndBlock),
      });
    });

    it("Owner changes treshold timestamp", async () => {
      const newTresholdTimestamp = currentTimestamp.add(new BN(20));
      result = await flammyFlameVault.changeThresholdTimestamp(newTresholdTimestamp, {
        from: alice,
      });
      expectEvent(result, "NewThresholdTimestamp", {
        thresholdTimestamp: String(newTresholdTimestamp),
      });
    });

    it("Owner changes number points", async () => {
      const newNumberPoints = 30092020;
      result = await flammyFlameVault.changeNumberPoints(String(newNumberPoints), {
        from: alice,
      });
      expectEvent(result, "NewNumberPoints", {
        numberPoints: String(newNumberPoints),
      });
    });

    it("Owner changes campaignId", async () => {
      const newCampaignId = "987654321";
      result = await flammyFlameVault.changeCampaignId(newCampaignId, {
        from: alice,
      });
      expectEvent(result, "NewCampaignId", {
        campaignId: String(newCampaignId),
      });
    });

    it("Only owner can call ownable functions changes end block", async () => {
      await expectRevert(
        flammyFlameVault.changeEndBlock(105, {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );
    });

    it("Only owner can call ownable functions changes treshold timestamp", async () => {
      await expectRevert(
        flammyFlameVault.changeThresholdTimestamp("123456789", {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );
    });

    it("Only owner can call ownable functions changes number points", async () => {
      await expectRevert(
        flammyFlameVault.changeNumberPoints("30092020", {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );
    });

    it("Only owner can call ownable functions changes campaignId", async () => {
      await expectRevert(
        flammyFlameVault.changeCampaignId("987654321", {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );
    });
  });

  describe("FlammySpecialPrediction", async () => {
    it("Contract is deployed", async () => {
      currentBlockNumber = await time.latestBlock();
      currentTimestamp = await time.latest();
      const _endBlockNumberS4 = currentBlockNumber.add(new BN(250));

      // Deploy FlammyPrediction
      flammyPrediction = await FlammyPrediction.new(
        prediction.address,
        flammyMintingStation.address,
        donaswapProfile.address,
        _endBlockNumberS4,
        1,
        _numberPoints,
        _campaignId,
        "predictionFlammy.json",
        { from: alice }
      );

      // Grants point role to the new contract
      result = await donaswapProfile.grantRole(POINT_ROLE, flammyPrediction.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: POINT_ROLE,
        account: flammyPrediction.address,
        sender: alice,
      });

      // Grant minting role to the new contract
      result = await flammyMintingStation.grantRole(MINTER_ROLE, flammyPrediction.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: MINTER_ROLE,
        account: flammyPrediction.address,
        sender: alice,
      });

      // Start first round of Prediction.
      await prediction.genesisStartRound();
    });

    it("Bob can mint", async () => {
      // Participate in prediction
      await prediction.betBull({ from: bob, value: parseEther("2").toString() });

      assert.equal(await flammyPrediction.canClaim(bob), true);

      result = await flammyPrediction.mintNFT({ from: bob });

      expectEvent(result, "FlammyMint", {
        to: bob,
        tokenId: "11",
        flammyId: "17",
      });

      expectEvent.inTransaction(result.receipt.transactionHash, donaswapProfile, "UserPointIncrease", {
        userAddress: bob,
        numberPoints: String(_numberPoints),
        campaignId: String(_campaignId),
      });

      assert.equal(await flammyPrediction.canClaim(bob), false);
    });

    it("Bob cannot mint twice", async () => {
      await expectRevert(flammyPrediction.mintNFT({ from: bob }), "ERR_HAS_CLAIMED");
    });

    it("Eve cannot mint as she doesn't have profile", async () => {
      assert.equal(await flammyPrediction.canClaim(eve), false);

      await expectRevert(flammyPrediction.mintNFT({ from: eve }), "Not registered");
    });

    it("Carol cannot mint without an active profile", async () => {
      // Participate in prediction
      await prediction.betBull({ from: carol, value: parseEther("2").toString() });

      assert.equal(await flammyPrediction.canClaim(carol), true);

      await donaswapProfile.pauseProfile({ from: carol });

      await expectRevert(flammyPrediction.mintNFT({ from: carol }), "ERR_USER_NOT_ACTIVE");

      assert.equal(await flammyPrediction.canClaim(carol), false);
    });

    it("Carol can mint with an active profile", async () => {
      await donaswapFlammies.approve(donaswapProfile.address, "2", {
        from: carol,
      });

      await donaswapProfile.reactivateProfile(donaswapFlammies.address, "2", {
        from: carol,
      });

      assert.equal(await flammyPrediction.canClaim(carol), true);

      result = await flammyPrediction.mintNFT({ from: carol });

      expectEvent(result, "FlammyMint", {
        to: carol,
        tokenId: "12",
        flammyId: "17",
      });

      expectEvent.inTransaction(result.receipt.transactionHash, donaswapProfile, "UserPointIncrease", {
        userAddress: carol,
        numberPoints: String(_numberPoints),
        campaignId: String(_campaignId),
      });

      assert.equal(await flammyPrediction.canClaim(carol), false);
    });

    it("Eve cannot mint as she did not participate in prediction", async () => {
      // Eve mints FLAME/NFT/creates a profile (userId = 5)
      await mockFLAME.mintTokens(parseEther("100"), { from: eve });

      assert.equal(await flammyPrediction.canClaim(eve), false);

      await mockFLAME.approve(flammyFactoryV3.address, parseEther("100"), {
        from: eve,
      });

      result = await flammyFactoryV3.mintNFT(9, { from: eve });

      expectEvent(result, "FlammyMint", {
        to: eve,
        tokenId: "13",
        flammyId: "9",
      });

      await donaswapFlammies.approve(donaswapProfile.address, "13", {
        from: eve,
      });

      await mockFLAME.approve(donaswapProfile.address, parseEther("100"), {
        from: eve,
      });

      await donaswapProfile.createProfile("1", donaswapFlammies.address, "13", {
        from: eve,
      });

      assert.equal(await flammyPrediction.canClaim(eve), false);

      await expectRevert(flammyPrediction.mintNFT({ from: eve }), "ERR_USER_NOT_ELIGIBLE");
    });

    it("Eve cannot mint as he participated too late in the prediction", async () => {
      // Advance blocks in order to go out of range
      await time.advanceBlockTo(currentBlockNumber.add(new BN(101)));

      // Launch new rounds in order not to be eligible
      // Note: Pausing/Relaunching is the fastest way so we do not need to Mock Oracle, etc...
      await prediction.pause({ from: alice });
      await prediction.unpause({ from: alice });
      await prediction.genesisStartRound({ from: alice });

      // Participate in Prediction
      await prediction.betBear({ from: eve, value: parseEther("1").toString() });

      assert.equal(await flammyPrediction.canClaim(eve), false);

      await expectRevert(flammyPrediction.mintNFT({ from: eve }), "ERR_USER_NOT_ELIGIBLE");
    });

    it("David cannot mint as end block has passed", async () => {
      currentBlockNumber = await time.latestBlock();
      await time.advanceBlockTo(currentBlockNumber.add(new BN(150)));
      await expectRevert(flammyPrediction.mintNFT({ from: david }), "TOO_LATE");
    });

    it("Owner changes end block", async () => {
      const newEndBlock = currentBlockNumber.add(new BN(25));
      result = await flammyPrediction.changeEndBlock(newEndBlock, {
        from: alice,
      });
      expectEvent(result, "NewEndBlock", {
        endBlock: String(newEndBlock),
      });
    });

    it("Owner changes treshold round", async () => {
      const newTresholdRound = new BN(25);
      result = await flammyPrediction.changeThresholdRound(newTresholdRound, {
        from: alice,
      });
      expectEvent(result, "NewThresholdRound", {
        thresholdRound: String(newTresholdRound),
      });
    });

    it("Owner changes number points", async () => {
      const newNumberPoints = 30092020;
      result = await flammyPrediction.changeNumberPoints(String(newNumberPoints), {
        from: alice,
      });
      expectEvent(result, "NewNumberPoints", {
        numberPoints: String(newNumberPoints),
      });
    });

    it("Owner changes campaignId", async () => {
      const newCampaignId = "987654321";
      result = await flammyPrediction.changeCampaignId(newCampaignId, {
        from: alice,
      });
      expectEvent(result, "NewCampaignId", {
        campaignId: String(newCampaignId),
      });
    });

    it("Only owner can call ownable functions changes end block", async () => {
      await expectRevert(
        flammyPrediction.changeEndBlock(105, {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );
    });

    it("Only owner can call ownable functions changes treshold round", async () => {
      await expectRevert(
        flammyPrediction.changeThresholdRound("123456789", {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );
    });

    it("Only owner can call ownable functions changes number points", async () => {
      await expectRevert(
        flammyPrediction.changeNumberPoints("30092020", {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );
    });

    it("Only owner can call ownable functions changes campaignId", async () => {
      await expectRevert(
        flammyPrediction.changeCampaignId("987654321", {
          from: bob,
        }),
        "Ownable: caller is not the owner"
      );
    });
  });

  describe("FlammySpecialLottery", async () => {
    const NFT_ID_1 = 18;
    const NFT_ID_2 = 19;
    const NFT_ID_3 = 20;
    const LOTTERY_ID = 1;

    it("Contract is deployed", async () => {
      currentBlockNumber = await time.latestBlock();
      currentTimestamp = await time.latest();

      // Setup profile contract
      const _numberFlameToReactivate = parseEther("2"); // 2 FLAME
      const _numberFlameToRegister = parseEther("1"); // 1 FLAME
      const _numberFlameToUpdate = parseEther("2"); // 2 FLAME

      donaswapProfile = await DonaswapProfile.new(
        mockFLAME.address,
        _numberFlameToReactivate,
        _numberFlameToRegister,
        _numberFlameToUpdate,
        { from: alice }
      );

      // Donaswap Profile roles.
      POINT_ROLE = await donaswapProfile.POINT_ROLE();
      await donaswapProfile.addTeam("The Testers", "ipfs://hash/team1.json", {
        from: alice,
      });
      await donaswapProfile.addNftAddress(mockFlammies.address, {
        from: alice,
      });

      // Bob's profile
      await mockFlammies.mint({ from: bob });
      const bobProfileNftId = "0";
      await mockFlammies.approve(donaswapProfile.address, bobProfileNftId, {
        from: bob,
      });
      await mockFLAME.approve(donaswapProfile.address, parseEther("500"), {
        from: bob,
      });
      await donaswapProfile.createProfile("1", mockFlammies.address, bobProfileNftId, {
        from: bob,
      });

      // Carol's profile
      await mockFlammies.mint({ from: carol });
      const carolProfileNftId = "1";
      await mockFlammies.approve(donaswapProfile.address, carolProfileNftId, {
        from: carol,
      });
      await mockFLAME.approve(donaswapProfile.address, parseEther("500"), {
        from: carol,
      });
      await donaswapProfile.createProfile("1", mockFlammies.address, carolProfileNftId, {
        from: carol,
      });

      // David's profile
      await mockFlammies.mint({ from: david });
      const davidProfileNftId = "2";
      await mockFlammies.approve(donaswapProfile.address, davidProfileNftId, {
        from: david,
      });
      await mockFLAME.approve(donaswapProfile.address, parseEther("500"), {
        from: david,
      });
      await donaswapProfile.createProfile("1", mockFlammies.address, davidProfileNftId, {
        from: david,
      });

      // Lottery setup
      flammySpecialLottery = await FlammySpecialLottery.new(
        donaswapLottery.address,
        flammyMintingStation.address,
        donaswapProfile.address,
        currentBlockNumber + 10000,
        "flammyLottery1.json",
        "flammyLottery2.json",
        "flammyLottery3.json",
        _numberPoints,
        _numberPoints + 1,
        _numberPoints + 2,
        _campaignId,
        _campaignId + 1,
        _campaignId + 2,
        1,
        10
      );

      // Grants point role to the new contract
      result = await donaswapProfile.grantRole(POINT_ROLE, flammySpecialLottery.address, {
        from: alice,
      });
      expectEvent(result, "RoleGranted", {
        role: POINT_ROLE,
        account: flammySpecialLottery.address,
        sender: alice,
      });
      // Grant minting role to the new contract
      result = await flammyMintingStation.grantRole(MINTER_ROLE, flammySpecialLottery.address, {
        from: alice,
      });
      expectEvent(result, "RoleGranted", {
        role: MINTER_ROLE,
        account: flammySpecialLottery.address,
        sender: alice,
      });

      await donaswapLottery.setOperatorAndTreasuryAndInjectorAddresses(alice, alice, alice, { from: alice });

      const _lengthLottery = new BN("60"); // In Sec
      const _priceTicketInFlame = parseEther("0.1");
      const _discountDivisor = "2000";
      const _rewardsBreakdown = ["200", "300", "500", "1500", "2500", "5000"];
      const _treasuryFee = "2000";
      await donaswapLottery.startLottery(
        new BN(await time.latest()).add(_lengthLottery),
        _priceTicketInFlame,
        _discountDivisor,
        _rewardsBreakdown,
        _treasuryFee,
        { from: alice }
      );

      // bob didn't played
      // carol played
      // david won
      // frank won without a profile
      await donaswapLottery.buyTickets(LOTTERY_ID, [1634660, 1999998], { from: carol });
      await donaswapLottery.buyTickets(LOTTERY_ID, [1634660, 1999999], { from: david });
      await donaswapLottery.buyTickets(LOTTERY_ID, [1999999], { from: frank });

      await donaswapLottery.closeLottery(LOTTERY_ID, { from: alice });
      await donaswapLottery.drawFinalNumberAndMakeLotteryClaimable(LOTTERY_ID, true, { from: alice });
      await donaswapLottery.claimTickets(LOTTERY_ID, ["3"], ["5"], { from: david });
      await donaswapLottery.claimTickets(LOTTERY_ID, ["4"], ["5"], { from: frank });
    });

    it("Owner can whitelist for NFT3", async () => {
      const whitelist = [eve];
      const res = await flammySpecialLottery.whitelistAddresses(whitelist, {
        from: alice,
      });
      expectEvent(res, "NewAddressWhitelisted", {
        users: whitelist,
      });
    });

    // Can claim

    it("Bob can't claim any NFT", async () => {
      const canClaimNft1 = await flammySpecialLottery.canClaimNft1(bob, LOTTERY_ID, { from: bob });
      const canClaimNft2 = await flammySpecialLottery.canClaimNft2(bob, LOTTERY_ID, 0, { from: bob });
      const canClaimNft3 = await flammySpecialLottery.canClaimNft3(bob, { from: bob });

      assert.isFalse(canClaimNft1);
      assert.isFalse(canClaimNft2);
      assert.isFalse(canClaimNft3);
    });

    it("Carol can only claim NFT1", async () => {
      const canClaimNft1 = await flammySpecialLottery.canClaimNft1(carol, LOTTERY_ID, { from: carol });
      const canClaimNft2 = await flammySpecialLottery.canClaimNft2(carol, LOTTERY_ID, 0, { from: carol });
      const canClaimNft3 = await flammySpecialLottery.canClaimNft3(carol, { from: carol });

      assert.isTrue(canClaimNft1);
      assert.isFalse(canClaimNft2);
      assert.isFalse(canClaimNft3);
    });

    it("David can only claim NFT1 and NFT2", async () => {
      const canClaimNft1 = await flammySpecialLottery.canClaimNft1(david, LOTTERY_ID, { from: david });
      const canClaimNft2 = await flammySpecialLottery.canClaimNft2(david, LOTTERY_ID, 1, { from: david });
      const canClaimNft3 = await flammySpecialLottery.canClaimNft3(david, { from: david });

      assert.isTrue(canClaimNft1);
      assert.isTrue(canClaimNft2);
      assert.isFalse(canClaimNft3);
    });

    it("Frank can't claim any NFT", async () => {
      const canClaimNft1 = await flammySpecialLottery.canClaimNft1(frank, LOTTERY_ID, { from: frank });
      const canClaimNft2 = await flammySpecialLottery.canClaimNft2(frank, LOTTERY_ID, 0, { from: frank });
      const canClaimNft3 = await flammySpecialLottery.canClaimNft3(frank, { from: frank });

      assert.isFalse(canClaimNft1);
      assert.isFalse(canClaimNft2);
      assert.isFalse(canClaimNft3);
    });

    it("Carol can claim NFT3", async () => {
      await flammySpecialLottery.whitelistAddresses([carol], {
        from: alice,
      });
      const canClaimNft3 = await flammySpecialLottery.canClaimNft3(carol, { from: carol });
      assert.isTrue(canClaimNft3);
    });

    // Mint
    it("Can't mint if the lottery is out of range", async () => {
      await expectRevert(flammySpecialLottery.mintNFT(NFT_ID_1, 2, 0, { from: carol }), "User: Not eligible");
    });

    it("Can't mint if the nft id is out of range", async () => {
      await expectRevert(flammySpecialLottery.mintNFT(1, LOTTERY_ID, 0, { from: carol }), "NFT: Id out of range");
    });

    it("Carol can mint NFT1 and NFT3", async () => {
      const res1 = await flammySpecialLottery.mintNFT(NFT_ID_1, LOTTERY_ID, 0, { from: carol });
      expectEvent(res1, "FlammyMint", {
        to: carol,
        tokenId: "14",
        flammyId: NFT_ID_1.toString(),
      });

      await expectRevert(flammySpecialLottery.mintNFT(NFT_ID_2, LOTTERY_ID, 0, { from: carol }), "User: Not eligible");

      const res2 = await flammySpecialLottery.mintNFT(NFT_ID_3, LOTTERY_ID, 0, { from: carol });
      expectEvent(res2, "FlammyMint", {
        to: carol,
        tokenId: "15",
        flammyId: NFT_ID_3.toString(),
      });
    });

    it("David can mint NFT1 and NFT2", async () => {
      const res1 = await flammySpecialLottery.mintNFT(NFT_ID_1, LOTTERY_ID, 1, { from: david });
      expectEvent(res1, "FlammyMint", {
        to: david,
        tokenId: "16",
        flammyId: NFT_ID_1.toString(),
      });

      const res2 = await flammySpecialLottery.mintNFT(NFT_ID_2, LOTTERY_ID, 1, { from: david });
      expectEvent(res2, "FlammyMint", {
        to: david,
        tokenId: "17",
        flammyId: NFT_ID_2.toString(),
      });

      await expectRevert(flammySpecialLottery.mintNFT(NFT_ID_3, LOTTERY_ID, 1, { from: david }), "User: Not eligible");
    });

    it("Frank can't mint any NFT", async () => {
      await expectRevert(flammySpecialLottery.mintNFT(NFT_ID_1, LOTTERY_ID, 1, { from: frank }), "User: Not eligible");
      await expectRevert(flammySpecialLottery.mintNFT(NFT_ID_2, LOTTERY_ID, 1, { from: frank }), "User: Not eligible");
      await expectRevert(flammySpecialLottery.mintNFT(NFT_ID_3, LOTTERY_ID, 1, { from: frank }), "User: Not eligible");
    });

    // Owner tests

    it("Owner can change the end block", async () => {
      const newEndBlock = 1000000000;
      const res = await flammySpecialLottery.changeEndBlock(newEndBlock, {
        from: alice,
      });
      expectEvent(res, "NewEndBlock", {
        endBlock: newEndBlock.toString(),
      });
      const block = await flammySpecialLottery.endBlock();
      assert.equal(block, newEndBlock);
    });

    it("Owner can change the campaign id", async () => {
      const newFlammyId = 18;
      const newCampaignId = 2;
      const res = await flammySpecialLottery.changeCampaignId(newFlammyId, newCampaignId, {
        from: alice,
      });
      expectEvent(res, "NewCampaignId", {
        flammyId: newFlammyId.toString(),
        campaignId: newCampaignId.toString(),
      });
      const id = await flammySpecialLottery.campaignIds(newFlammyId);
      assert.equal(id, newCampaignId);
    });

    it("Owner can change the number of points", async () => {
      const newFlammyId = 19;
      const newNumberPoints = 2;
      const res = await flammySpecialLottery.changeNumberPoints(newFlammyId, newNumberPoints, {
        from: alice,
      });
      expectEvent(res, "NewNumberPoints", {
        flammyId: newFlammyId.toString(),
        numberPoints: newNumberPoints.toString(),
      });
      const nbPoints = await flammySpecialLottery.numberPoints(newFlammyId);
      assert.equal(nbPoints, newNumberPoints);
    });

    it("Owner can change the token URI", async () => {
      const NEW_URI = "shit_uri";
      const res = await flammySpecialLottery.changeTokenURI(NFT_ID_1, NEW_URI, {
        from: alice,
      });
      expectEvent(res, "NewTokenURI", {
        flammyId: NFT_ID_1.toString(),
        tokenURI: NEW_URI.toString(),
      });
      const uri = await flammySpecialLottery.tokenURIs(NFT_ID_1);
      assert.equal(uri, NEW_URI);
    });

    it("Owner can change the lottery rounds", async () => {
      const newStartRound = 2;
      const newEndRound = 9;
      await expectRevert(
        flammySpecialLottery.changeLotteryRounds(newEndRound, newStartRound, { from: alice }),
        "Round: startLotteryRound > finalLotteryRound"
      );

      const res = await flammySpecialLottery.changeLotteryRounds(newStartRound, newEndRound, {
        from: alice,
      });
      expectEvent(res, "NewLotteryRounds", {
        startLotteryRound: newStartRound.toString(),
        finalLotteryRound: newEndRound.toString(),
      });

      const startRound = await flammySpecialLottery.startLotteryRound();
      assert.equal(startRound, newStartRound);
      const endRound = await flammySpecialLottery.finalLotteryRound();
      assert.equal(endRound, newEndRound);
    });
  });

  describe("FlammySpecialLottery", async () => {
    const NFT_ID = 21;

    it("Contract is deployed and role given", async () => {
      currentBlockNumber = await time.latestBlock();

      flammySpecialAdmin = await FlammySpecialAdmin.new(
        flammyMintingStation.address,
        currentBlockNumber + 100,
        "flammyAdmin.json",
        { from: alice }
      );

      const res = await flammyMintingStation.grantRole(MINTER_ROLE, flammySpecialAdmin.address, {
        from: alice,
      });

      expectEvent(res, "RoleGranted", {
        role: MINTER_ROLE,
        account: flammySpecialAdmin.address,
        sender: alice,
      });
    });

    it("Owner can whitelist for NFT", async () => {
      assert.equal(await flammySpecialAdmin.canClaim(bob), false);
      assert.equal(await flammySpecialAdmin.canClaim(carol), false);
      assert.equal(await flammySpecialAdmin.canClaim(david), false);

      const whitelist = [bob, carol, david];
      const res = await flammySpecialAdmin.whitelistAddresses(whitelist, {
        from: alice,
      });
      expectEvent(res, "NewAddressesWhitelisted", {
        users: whitelist,
      });

      assert.equal(await flammySpecialAdmin.canClaim(bob), true);
      assert.equal(await flammySpecialAdmin.canClaim(carol), true);
      assert.equal(await flammySpecialAdmin.canClaim(david), true);
    });

    it("Owner can unwhitelist for NFT", async () => {
      const whitelist = [david];
      const res = await flammySpecialAdmin.unwhitelistAddresses(whitelist, {
        from: alice,
      });

      expectEvent(res, "NewAddressesUnwhitelisted", {
        users: whitelist,
      });

      assert.equal(await flammySpecialAdmin.canClaim(david), false);
    });

    it("Bob can mint", async () => {
      const res = await flammySpecialAdmin.mintNFT({ from: bob });

      expectEvent(res, "FlammyMint", {
        to: bob,
        tokenId: "18",
        flammyId: "21",
      });

      assert.equal(await donaswapFlammies.tokenURI("18"), "ipfs://flammyAdmin.json");
      assert.equal(await flammySpecialAdmin.canClaim(bob), false);
      assert.equal(await flammySpecialAdmin.hasClaimed(bob), true);
    });

    it("Bob cannot mint twice", async () => {
      await expectRevert(flammySpecialAdmin.mintNFT({ from: bob }), "Claim: Already claimed");
    });

    it("David cannot mint as he is not whitelisted", async () => {
      await expectRevert(flammySpecialAdmin.mintNFT({ from: david }), "Claim: Not eligible");
    });

    it("Owner changes endBlock", async () => {
      currentBlockNumber = await time.latestBlock();

      const newEndBlock = currentBlockNumber.sub(new BN("2"));

      assert.equal(await flammySpecialAdmin.canClaim(carol), true);

      const res = await flammySpecialAdmin.changeEndBlock(newEndBlock, {
        from: alice,
      });

      expectEvent(res, "NewEndBlock", {
        endBlock: newEndBlock,
      });

      assert.equal(await flammySpecialAdmin.canClaim(carol), false);
    });

    it("Carol cannot mint as it is too late", async () => {
      await expectRevert(flammySpecialAdmin.mintNFT({ from: carol }), "Claim: Too late");
    });
  });

  describe("AnniversaryAchievement", async () => {
    it("Contract is deployed and role given", async () => {
      currentBlockNumber = await time.latestBlock();

      anniversaryAchievement = await AnniversaryAchievement.new(
        donaswapProfile.address,
        "100",
        "5",
        "511010101",
        currentBlockNumber + 100,
        { from: alice }
      );

      // Grants point role to the new contract
      result = await donaswapProfile.grantRole(POINT_ROLE, anniversaryAchievement.address, {
        from: alice,
      });

      expectEvent(result, "RoleGranted", {
        role: POINT_ROLE,
        account: anniversaryAchievement.address,
        sender: alice,
      });
    });

    it("Cannot claim anniversary if no profile or not enough point", async () => {
      assert.equal(await anniversaryAchievement.canClaim(alice), false);
      assert.equal(await anniversaryAchievement.canClaim(bob), false);
      assert.equal(await anniversaryAchievement.canClaim(carol), true);

      await expectRevert(anniversaryAchievement.claimAnniversaryPoints({ from: alice }), "Claim: Cannot claim");
      await expectRevert(anniversaryAchievement.claimAnniversaryPoints({ from: bob }), "Claim: Cannot claim");
    });

    it("Can claim anniversary", async () => {
      assert.equal(await anniversaryAchievement.canClaim(carol), true);

      result = await anniversaryAchievement.claimAnniversaryPoints({ from: carol });

      expectEvent.inTransaction(result.receipt.transactionHash, donaswapProfile, "UserPointIncrease", {
        userAddress: carol,
        numberPoints: "100",
        campaignId: "511010101",
      });

      assert.equal(await anniversaryAchievement.hasClaimed(carol), true);
      assert.equal(await anniversaryAchievement.canClaim(carol), false);
    });

    it("Cannot claim twice", async () => {
      await expectRevert(anniversaryAchievement.claimAnniversaryPoints({ from: carol }), "Claim: Cannot claim");
    });

    it("Only owner can call owner function", async () => {
      await expectRevert(
        anniversaryAchievement.changeNumberPointsAndThreshold("1", "1", { from: bob }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(anniversaryAchievement.changeEndBlock("1", { from: bob }), "Ownable: caller is not the owner");

      await expectRevert(
        anniversaryAchievement.changeCampaignId("1", { from: bob }),
        "Ownable: caller is not the owner"
      );
    });

    it("Owner updates points threshold", async () => {
      result = await anniversaryAchievement.changeNumberPointsAndThreshold("100", "0", { from: alice });
      expectEvent(result, "NewNumberPointsAndThreshold", { numberPoints: "100", thresholdPoints: "0" });
      assert.equal(await anniversaryAchievement.canClaim(alice), false);
      assert.equal(await anniversaryAchievement.canClaim(bob), true);
    });

    it("Owner can update end block", async () => {
      result = await anniversaryAchievement.changeEndBlock(currentBlockNumber, { from: alice });
      expectEvent(result, "NewEndBlock", { endBlock: currentBlockNumber.toString() });
      assert.equal(await anniversaryAchievement.canClaim(bob), false);
    });

    it("Owner can change campaignId", async () => {
      result = await anniversaryAchievement.changeCampaignId("511", { from: alice });
      expectEvent(result, "NewCampaignId", { campaignId: "511" });
    });
  });
});
