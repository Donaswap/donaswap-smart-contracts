# Donaswap Profile & Flammies

## Description

An open system for users to bind their address with a customizable profile on Donaswap.

## Docs

### DonaswapProfile and CampaignIds

- [DonaswapProfile - Description](docs/DonaswapProfile.md)
- [CampaignIds - Proposed Logic](docs/CampaignIDs.md)

### Point apps (IFO, TradingComp)

- [PointCenterIFO - Description](docs/PointCenterIFO.md)
- [TradingCompV1 - Description](docs/TradingCompV1.md)

### NFTs

- [FlammyFactoryV3](/docs/FlammyFactoryV3.md): the factory contract that mints "starter NFT collectibles"
- [FlammyMintingStation](/docs/FlammyFactoryStation.md): the owner of `DonaswapFlammies` that grants minting roles to other factories
- [FlammySpecialV1](/docs/FlammySpecialV1.md): a special factory contract to mint flammies based on (1) a FLAME price (2) when the user joined the `DonaswapProfile` system
- [FlammySpecialPrediction](docs/FlammySpecialPrediction.md): a factory contract to mint flammies if the user joined the `Prediction` beta
- [FlammySpecialFlameVault](docs/FlammySpecialFlameVault.md): a factory contract to mint flammies if the user used `FlameVault` feature
- [DonaswapFlammies](/docs/DonaswapFlammies.md): the ERC721 NFT contract that contains Donaswap flammies

#### NFTs (deprecated)

- [FlammyFactoryV2](docs/FlammyFactoryV2.md)
