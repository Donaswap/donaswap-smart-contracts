import { assert } from "chai";
import { expectEvent, expectRevert, time } from "@openzeppelin/test-helpers";
import { artifacts, contract, ethers } from "hardhat";

const MockBEP20 = artifacts.require("./utils/MockBEP20.sol");
const FlammyFactoryV2 = artifacts.require("./archive/FlammyFactoryV2.sol");
const FlammyMintingFarm = artifacts.require("./FlammyMintingFarm.sol");
const DonaswapFlammies = artifacts.require("./DonaswapFlammies.sol");

contract("FlammyMintingFarm", ([alice, bob, carol, david, erin, frank, minterTester]) => {
  let mockFLAME,
    donaswapFlammies,
    donaswapFlammiesAddress,
    flammyMintingFarm,
    flammyFactoryV2,
    flammyMintingFarmAddress,
    result,
    result2;

  before(async () => {
    let _totalSupplyDistributed = 5;
    let _flamePerBurn = 20;
    let _testBaseURI = "ipfs://ipfs/";
    let _ipfsHash = "IPFSHASH/";
    let _endBlockTime = 150;

    mockFLAME = await MockBEP20.new("Donaswap Mock Token", "FLAME", 10000, {
      from: minterTester,
    });

    flammyMintingFarm = await FlammyMintingFarm.new(
      mockFLAME.address,
      _totalSupplyDistributed,
      _flamePerBurn,
      _testBaseURI,
      _ipfsHash,
      _endBlockTime,
      { from: alice }
    );

    flammyMintingFarmAddress = flammyMintingFarm.address;
    donaswapFlammiesAddress = await flammyMintingFarm.donaswapFlammies();
    donaswapFlammies = await DonaswapFlammies.at(donaswapFlammiesAddress);
  });

  // Check ticker, symbols, supply, and owner are correct
  describe("All contracts are deployed correctly", async () => {
    it("Symbol is correct", async () => {
      result = await donaswapFlammies.symbol();
      assert.equal(result, "PB");
    });
    it("Name is correct", async () => {
      result = await donaswapFlammies.name();
      assert.equal(result, "Donaswap Flammies");
    });
    it("Total supply + number of NFT distributed is 0", async () => {
      result = await donaswapFlammies.totalSupply();
      assert.equal(result, 0);
      result = await flammyMintingFarm.totalSupplyDistributed();
      assert.equal(result, 5);
      result = await flammyMintingFarm.currentDistributedSupply();
      assert.equal(result, 0);
    });
    it("Owner is the FlammyMintingFarm contract", async () => {
      result = await donaswapFlammies.owner();
      assert.equal(result, flammyMintingFarmAddress);
      result = await flammyMintingFarm.owner();
      assert.equal(result, alice);
    });
    it("MinterTester distributes tokens to accounts", async () => {
      result = await mockFLAME.totalSupply();
      assert.equal(result, 10000);
      result = await mockFLAME.balanceOf(minterTester);
      assert.equal(result, 10000);
      // transfer FLAME to 5 accounts
      await mockFLAME.transfer(alice, 500, { from: minterTester });
      await mockFLAME.transfer(bob, 300, { from: minterTester });
      await mockFLAME.transfer(carol, 500, { from: minterTester });
      await mockFLAME.transfer(david, 500, { from: minterTester });
      await mockFLAME.transfer(erin, 400, { from: minterTester });
      result = await mockFLAME.balanceOf(minterTester);
      assert.equal(result, 7800);
    });
  });

  // Check ticker and symbols are correct
  describe("Whitelisting works as intended", async () => {
    it("Only contract owner can whitelist", async () => {
      // Only contract owner can whitelist
      await expectRevert(
        flammyMintingFarm.whitelistAddresses([alice, bob, carol, david, erin], { from: bob }),
        "Ownable: caller is not the owner"
      );

      // Whitelist addresses
      await flammyMintingFarm.whitelistAddresses([alice, bob, carol, david, erin], { from: alice });
    });
  });

  describe("Tokens can be claimed", async () => {
    it("Only FLAME owner can mint once", async () => {
      result = await flammyMintingFarm.hasClaimed(alice);
      assert.equal(result, false);

      result = await flammyMintingFarm.canClaim(alice);
      assert.equal(result, true);

      result = await flammyMintingFarm.mintNFT("3", { from: alice });

      // Obtain gas used from the receipt
      expectEvent(result, "FlammyMint", {
        to: alice,
        tokenId: "0",
        flammyId: "3",
      });

      // check totalSupply and FLAME balance of Alice
      result = await donaswapFlammies.totalSupply();
      assert.equal(result, 1);
      result = await donaswapFlammies.balanceOf(alice);
      assert.equal(result, 1);
      result = await donaswapFlammies.ownerOf("0");
      assert.equal(result, alice);
      result = await flammyMintingFarm.currentDistributedSupply();
      assert.equal(result, 1);
      result = await flammyMintingFarm.hasClaimed(alice);
      assert.equal(result, true);

      // Check how many exists for a specific flammyCount
      result = await donaswapFlammies.flammyCount("3");
      assert.equal(result, 1);

      // Check token URI is ok
      result = await donaswapFlammies.tokenURI("0");
      assert.equal(result, "ipfs://ipfs/IPFSHASH/circular.json");

      // verify Alice cannot claim twice
      await expectRevert(flammyMintingFarm.mintNFT("2", { from: alice }), "Has claimed");

      // verify someone needs to be whitelisted
      await expectRevert(flammyMintingFarm.mintNFT("2", { from: frank }), "Cannot claim");

      result = await flammyMintingFarm.canClaim(frank);
      assert.equal(result, false);

      // Frank is whitelisted by Alice
      await flammyMintingFarm.whitelistAddresses([frank, minterTester], {
        from: alice,
      });

      result = await flammyMintingFarm.canClaim(frank);
      assert.equal(result, true);

      // verify FLAME is required to mint the NFT
      await expectRevert(flammyMintingFarm.mintNFT("2", { from: frank }), "Must own FLAME");
      // Verify that only flammyIds strictly inferior to 5 are available (0, 1, 2, 3, 4)
      await expectRevert(flammyMintingFarm.mintNFT("5", { from: bob }), "flammyId unavailable");
    });

    it("It is not possible to mint more than expected", async () => {
      // 4 more accounts collect their NFTs
      await flammyMintingFarm.mintNFT("1", { from: bob });
      await flammyMintingFarm.mintNFT("2", { from: carol });
      await flammyMintingFarm.mintNFT("2", { from: david });
      await flammyMintingFarm.mintNFT("0", { from: erin });

      // Alice transfers 10 FLAME to Frank to verify he cannot participate
      await mockFLAME.transfer(frank, "10", { from: alice });
      await expectRevert(flammyMintingFarm.mintNFT("2", { from: frank }), "Nothing left");

      // Check that currentDistributedSupply is equal to totalSupplyDistributed
      result = await flammyMintingFarm.totalSupplyDistributed();
      result2 = await flammyMintingFarm.currentDistributedSupply();
      assert.equal(result.toString(), result2.toString());

      // Check how many exists for a specific flammyCount
      result = await donaswapFlammies.flammyCount("0");
      assert.equal(result, 1);
      result = await donaswapFlammies.flammyCount("1");
      assert.equal(result, 1);
      result = await donaswapFlammies.flammyCount("2");
      assert.equal(result, 2);
      result = await donaswapFlammies.flammyCount("3");
      assert.equal(result, 1);
      result = await donaswapFlammies.flammyCount("4");
      assert.equal(result, 0);
      result = await donaswapFlammies.flammyCount("5");
      assert.equal(result, 0);
    });

    it("Names and ids of Flammies are appropriate", async () => {
      result = await donaswapFlammies.getFlammyName(0);
      assert.equal(result, "Swapsies");
      result = await donaswapFlammies.getFlammyName(1);
      assert.equal(result, "Drizzle");
      result = await donaswapFlammies.getFlammyName(2);
      assert.equal(result, "Blueberries");
      result = await donaswapFlammies.getFlammyName(3);
      assert.equal(result, "Circular");
      result = await donaswapFlammies.getFlammyName(4);
      assert.equal(result, "Sparkle");

      result = await donaswapFlammies.getFlammyId(0);
      assert.equal(result, 3);
      result = await donaswapFlammies.getFlammyId(1);
      assert.equal(result, 1);
      result = await donaswapFlammies.getFlammyId(2);
      assert.equal(result, 2);
      result = await donaswapFlammies.getFlammyId(3);
      assert.equal(result, 2);
      result = await donaswapFlammies.getFlammyId(4);
      assert.equal(result, 0);

      result = await donaswapFlammies.getFlammyNameOfTokenId(0);
      assert.equal(result, "Circular");
      result = await donaswapFlammies.getFlammyNameOfTokenId(1);
      assert.equal(result, "Drizzle");
      result = await donaswapFlammies.getFlammyNameOfTokenId(2);
      assert.equal(result, "Blueberries");
      result = await donaswapFlammies.getFlammyNameOfTokenId(3);
      assert.equal(result, "Blueberries");
      result = await donaswapFlammies.getFlammyNameOfTokenId(4);
      assert.equal(result, "Swapsies");
    });

    it("Users can only burns tokens they own", async () => {
      // Alice transfers number flamePerBurn * totalSupplyDistributed to the contract
      result = await mockFLAME.transfer(flammyMintingFarmAddress, 100, {
        from: alice,
      });
      expectEvent(result, "Transfer", {
        from: alice,
        to: flammyMintingFarmAddress,
        value: "100",
      });

      // Verify that nothing was burnt
      result = await flammyMintingFarm.countFlammiesBurnt();
      assert.equal(result, "0");

      // Test that it fails without owning the token
      await expectRevert(flammyMintingFarm.burnNFT("1", { from: carol }), "Not the owner");

      result = await flammyMintingFarm.burnNFT("2", { from: carol });

      expectEvent(result, "FlammyBurn", {
        from: carol,
        tokenId: "2",
      });

      // Check that one NFT was burnt
      result = await flammyMintingFarm.countFlammiesBurnt();
      assert.equal(result, "1");

      // Check Carol has no NFT
      result = await donaswapFlammies.balanceOf(carol);
      assert.equal(result, 0);

      // Check FLAME balance of the account decreases
      result = await mockFLAME.balanceOf(flammyMintingFarmAddress);
      assert.equal(result, 80);
    });

    it("Alice only can withdraw after the time", async () => {
      // Alice tries to withdraw FLAME tokens before the end
      await expectRevert(flammyMintingFarm.withdrawFlame(80, { from: alice }), "too early");

      // move the current block to the _endBlockTime
      await time.advanceBlockTo(150);

      // Frank tries to steal it
      await expectRevert(flammyMintingFarm.withdrawFlame(80, { from: frank }), "Ownable: caller is not the owner");

      // Bob tries to burn his NFT (tokenId = 2)
      await expectRevert(flammyMintingFarm.burnNFT("1", { from: bob }), "too late");

      // Alice tries to withdraw more
      await expectRevert(
        flammyMintingFarm.withdrawFlame(2000, { from: alice }),
        "BEP20: transfer amount exceeds balance"
      );

      await flammyMintingFarm.withdrawFlame(80, { from: alice });

      result = await mockFLAME.balanceOf(alice);

      // Verify FLAME balance of Alice is updated
      assert.equal(result.toString(), "470"); // she gave 10 to Frank - 20 burn

      // Verify FLAME balance of FlammyMintingFarm contract is 0
      result = await mockFLAME.balanceOf(flammyMintingFarmAddress);
      assert.equal(result, 0);
    });
  });

  describe("FlammyFactoryV2", async () => {
    it("NFT contract cannot change owner by a third party", async () => {
      result2 = await donaswapFlammies.owner();

      // Check that only the owner of the flammyMintingFarm can call it
      await expectRevert(
        flammyMintingFarm.changeOwnershipNFTContract(carol, { from: bob }),
        "Ownable: caller is not the owner"
      );
    });

    it("NFT contract owner changes correctly", async () => {
      // Alice, the owner, calls to change the ownership of the DonaswapFlammies contract to Bob
      result = await flammyMintingFarm.changeOwnershipNFTContract(bob, {
        from: alice,
      });

      expectEvent(result, "OwnershipTransferred", {
        previousOwner: flammyMintingFarmAddress,
        newOwner: bob,
      });

      // Check the new owner
      result = await donaswapFlammies.owner();

      // Verify that the old owner is not the new owner
      assert.notEqual(result, result2);
      // Verify that the new owner is Bob
      assert.equal(result, bob);
    });
  });

  describe("FlammyMintingFactoryV2", async () => {
    it("FlammyMintingFactoryV2 is deployed", async () => {
      const _ipfsHash = "testIpfsHash/";
      const _tokenPrice = "4000000000000000000";
      const _endBlockNumber = 350;
      const _startBlockNumber = 1;

      flammyFactoryV2 = await FlammyFactoryV2.new(
        donaswapFlammies.address,
        mockFLAME.address,
        _tokenPrice,
        _ipfsHash,
        _startBlockNumber,
        _endBlockNumber,
        { from: alice }
      );

      // Transfer ownership to Alice
      result = await donaswapFlammies.transferOwnership(flammyFactoryV2.address, { from: bob });

      expectEvent(result, "OwnershipTransferred", {
        previousOwner: bob,
        newOwner: flammyFactoryV2.address,
      });

      // Check the new owner is FlammyFactoryV2
      assert.equal(await donaswapFlammies.owner(), flammyFactoryV2.address);
      assert.equal(await flammyFactoryV2.startBlockNumber(), _startBlockNumber);
      assert.equal(await flammyFactoryV2.endBlockNumber(), _endBlockNumber);
      assert.equal(await flammyFactoryV2.tokenPrice(), _tokenPrice);
    });

    it("Flammy Names and json extensions are set", async () => {
      result = await flammyFactoryV2.setFlammyNames("MyFlammy5", "MyFlammy6", "MyFlammy7", "MyFlammy8", "MyFlammy9", {
        from: alice,
      });

      result = await donaswapFlammies.getFlammyName("5");
      assert.equal(result, "MyFlammy5");

      result = await donaswapFlammies.getFlammyName("6");
      assert.equal(result, "MyFlammy6");

      result = await donaswapFlammies.getFlammyName("7");
      assert.equal(result, "MyFlammy7");

      result = await donaswapFlammies.getFlammyName("8");
      assert.equal(result, "MyFlammy8");

      result = await donaswapFlammies.getFlammyName("9");
      assert.equal(result, "MyFlammy9");

      result = await flammyFactoryV2.setFlammyJson("test5.json", "test6.json", "test7.json", "test8.json", "test9.json", {
        from: alice,
      });
    });

    it("Alice can mint", async () => {
      // Alice mints 10 FLAME
      await mockFLAME.mintTokens("10000000000000000000", { from: alice });

      // FLAME was not approved
      await expectRevert(flammyFactoryV2.mintNFT("6", { from: alice }), "BEP20: transfer amount exceeds allowance");

      result = await mockFLAME.approve(flammyFactoryV2.address, "10000000000000000000", { from: alice });

      expectEvent(result, "Approval");

      // Cannot mint old series
      await expectRevert(flammyFactoryV2.mintNFT("4", { from: alice }), "flammyId too low");

      // Cannot mint series that do not exist
      await expectRevert(flammyFactoryV2.mintNFT("10", { from: alice }), "flammyId too high");

      assert.equal(await flammyFactoryV2.hasClaimed(alice), false);

      result = await flammyFactoryV2.mintNFT("6", { from: alice });

      expectEvent(result, "FlammyMint", {
        to: alice,
        tokenId: "5",
        flammyId: "6",
      });

      result = await donaswapFlammies.totalSupply();
      assert.equal(result.toString(), "5");
      assert.equal(await flammyFactoryV2.hasClaimed(alice), true);

      result = await donaswapFlammies.flammyCount("6");
      assert.equal(result.toString(), "1");

      result = await donaswapFlammies.getFlammyNameOfTokenId("5");
      assert.equal(result, "MyFlammy6");

      result = await donaswapFlammies.getFlammyId("5");
      assert.equal(result, "6");

      result = await mockFLAME.balanceOf(flammyFactoryV2.address);
      assert.equal(result, "4000000000000000000");
    });
    it("Alice cannot mint twice", async () => {
      await expectRevert(flammyFactoryV2.mintNFT("7", { from: alice }), "Has claimed");
    });
    it("Alice cannot mint twice", async () => {
      await expectRevert(flammyFactoryV2.mintNFT("7", { from: alice }), "Has claimed");
    });
    it("Bob cannot mint if too early or too late", async () => {
      // Bob mints 10 FLAME
      for (let i = 0; i < 5; i++) {
        await mockFLAME.mintTokens("2000000000000000000", { from: bob });
      }

      await mockFLAME.approve(flammyFactoryV2.address, "10000000000000000000", {
        from: bob,
      });

      await flammyFactoryV2.setStartBlockNumber(352, { from: alice });

      // move the current block to the _endBlockTime
      await time.advanceBlockTo(350);

      await expectRevert(flammyFactoryV2.mintNFT("7", { from: bob }), "too early");

      await flammyFactoryV2.setEndBlockNumber(360, { from: alice });

      // move the current block to the _endBlockTime
      await time.advanceBlockTo(361);

      await expectRevert(flammyFactoryV2.mintNFT("7", { from: bob }), "too late");
    });
    it("Block number functions revert as expected", async () => {
      // Admin function
      await expectRevert(flammyFactoryV2.setStartBlockNumber(10, { from: alice }), "too short");

      await expectRevert(flammyFactoryV2.setEndBlockNumber(10, { from: alice }), "too short");

      await flammyFactoryV2.setStartBlockNumber(600, { from: alice });

      await expectRevert(flammyFactoryV2.setEndBlockNumber(600, { from: alice }), "must be > startBlockNumber");

      // move the current block to 600
      await time.advanceBlockTo(600);

      await flammyFactoryV2.setEndBlockNumber(1000, { from: alice });
    });

    it("Fee is changed and Bob mints", async () => {
      // 2 FLAME
      const _newPrice = "2000000000000000000";
      await flammyFactoryV2.updateTokenPrice(_newPrice, { from: alice });

      result = await flammyFactoryV2.mintNFT("8", { from: bob });

      expectEvent(result, "FlammyMint", {
        to: bob,
        tokenId: "6",
        flammyId: "8",
      });

      result = await donaswapFlammies.totalSupply();
      assert.equal(result.toString(), "6");

      assert.equal(await flammyFactoryV2.hasClaimed(bob), true);

      result = await donaswapFlammies.flammyCount("8");
      assert.equal(result.toString(), "1");

      result = await donaswapFlammies.getFlammyNameOfTokenId("6");
      assert.equal(result, "MyFlammy8");

      result = await donaswapFlammies.getFlammyId("6");
      assert.equal(result, "8");

      result = await mockFLAME.balanceOf(flammyFactoryV2.address);
      assert.equal(result, "6000000000000000000");
    });

    it("Alice can claim fee", async () => {
      await flammyFactoryV2.claimFee("6000000000000000000", { from: alice });
      result = await mockFLAME.balanceOf(flammyFactoryV2.address);
      assert.equal(result, "0");
    });

    it("Frank cannot access functions as he is not owner", async () => {
      await expectRevert(
        flammyFactoryV2.changeOwnershipNFTContract(frank, {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV2.claimFee("1", {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV2.setFlammyJson("a1", "a2", "a3", "a4", "a5", {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV2.setFlammyNames("a1", "a2", "a3", "a4", "a5", {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV2.setStartBlockNumber(1, {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV2.setEndBlockNumber(1, {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );

      await expectRevert(
        flammyFactoryV2.updateTokenPrice(0, {
          from: frank,
        }),
        "Ownable: caller is not the owner"
      );
    });

    it("Contract is transferred", async () => {
      // Transfer ownership to Bob
      result = await flammyFactoryV2.changeOwnershipNFTContract(bob, {
        from: alice,
      });

      // Verify events
      expectEvent.inTransaction(result.receipt.transactionHash, donaswapFlammies, "OwnershipTransferred");
      // Check the new owner is Bob
      assert.equal(await donaswapFlammies.owner(), bob);
    });
  });
});
