import { ethers, network, run } from "hardhat";
import config from "../config";
import { constants } from "@openzeppelin/test-helpers";

const main = async () => {
  // Get network name: hardhat, testnet or mainnet.
  const { name } = network;

  if (name == "mainnet") {
    if (!process.env.KEY_MAINNET) {
      throw new Error("Missing private key, refer to README 'Deployment' section");
    }
    if (!config.Admin[name] || config.Admin[name] === constants.ZERO_ADDRESS) {
      throw new Error("Missing admin address, refer to README 'Deployment' section");
    }
    if (!config.Treasury[name] || config.Treasury[name] === constants.ZERO_ADDRESS) {
      throw new Error("Missing treasury address, refer to README 'Deployment' section");
    }
    if (!config.Syrup[name] || config.Syrup[name] === constants.ZERO_ADDRESS) {
      throw new Error("Missing syrup address, refer to README 'Deployment' section");
    }
    if (!config.Flame[name] || config.Flame[name] === constants.ZERO_ADDRESS) {
      throw new Error("Missing syrup address, refer to README 'Deployment' section");
    }
    if (!config.MasterChef[name] || config.MasterChef[name] === constants.ZERO_ADDRESS) {
      throw new Error("Missing master address, refer to README 'Deployment' section");
    }
  }

  console.log("Deploying to network:", network);

  let flame, syrup, masterchef, admin, treasury;

  if (name == "mainnet") {
    admin = config.Admin[name];
    treasury = config.Treasury[name];
    flame = config.Flame[name];
    syrup = config.Syrup[name];
    masterchef = config.MasterChef[name];
  } else {
    console.log("Deploying mocks");
    const FlameContract = await ethers.getContractFactory("FlameToken");
    const SyrupContract = await ethers.getContractFactory("SyrupBar");
    const MasterChefContract = await ethers.getContractFactory("MasterChef");
    const currentBlock = await ethers.provider.getBlockNumber();

    if (name === "hardhat") {
      const [deployer] = await ethers.getSigners();
      admin = deployer.address;
      treasury = deployer.address;
    } else {
      admin = config.Admin[name];
      treasury = config.Treasury[name];
    }

    flame = (await FlameContract.deploy()).address;
    await flame.deployed();
    syrup = (await SyrupContract.deploy(flame)).address;
    await syrup.deployed();
    masterchef = (await MasterChefContract.deploy(flame, syrup, admin, ethers.BigNumber.from("1"), currentBlock))
      .address;

    await masterchef.deployed();

    console.log("Admin:", admin);
    console.log("Treasury:", treasury);
    console.log("Flame deployed to:", flame);
    console.log("Syrup deployed to:", syrup);
    console.log("MasterChef deployed to:", masterchef);
  }

  console.log("Deploying Flame Vault...");

  const FlameVaultContract = await ethers.getContractFactory("FlameVault");
  const flameVault = await FlameVaultContract.deploy(flame, syrup, masterchef, admin, treasury);
  await flameVault.deployed();

  console.log("FlameVault deployed to:", flameVault.address);
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
