import { ethers, network } from "hardhat";
import config from "../../config";

const currentNetwork = network.name;

const main = async () => {
  console.log("Deploying to network:", currentNetwork);

  const _thresholdTimeStamp = "";
  const _endBlock = "";
  const _numberPoints = "";
  const _campaignId = "";
  const _tokenURI = "";

  const FlammySpecialFlameVault = await ethers.getContractFactory("FlammySpecialFlameVault");

  const flammySpecialFlameVault = await FlammySpecialFlameVault.deploy(
    config.FlameVault[currentNetwork],
    config.FlammyMintingStation[currentNetwork],
    config.DonaswapProfile[currentNetwork],
    _endBlock,
    _thresholdTimeStamp,
    _numberPoints,
    _campaignId,
    _tokenURI
  );

  await flammySpecialFlameVault.deployed();
  console.log("FlammySpecialFlameVault deployed to:", flammySpecialFlameVault.address);
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
