// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

/** @title DonaswapFlammies.
 * @notice It is the contracts for Donaswap NFTs.
 */
contract DonaswapFlammies is ERC721, Ownable {
    using Counters for Counters.Counter;

    // Map the number of tokens per flammyId
    mapping(uint8 => uint256) public flammyCount;

    // Map the number of tokens burnt per flammyId
    mapping(uint8 => uint256) public flammyBurnCount;

    // Used for generating the tokenId of new NFT minted
    Counters.Counter private _tokenIds;

    // Map the flammyId for each tokenId
    mapping(uint256 => uint8) private flammyIds;

    // Map the flammyName for a tokenId
    mapping(uint8 => string) private flammyNames;

    constructor(string memory _baseURI) public ERC721("Donaswap Flammies", "PB") {
        _setBaseURI(_baseURI);
    }

    /**
     * @dev Get flammyId for a specific tokenId.
     */
    function getFlammyId(uint256 _tokenId) external view returns (uint8) {
        return flammyIds[_tokenId];
    }

    /**
     * @dev Get the associated flammyName for a specific flammyId.
     */
    function getFlammyName(uint8 _flammyId) external view returns (string memory) {
        return flammyNames[_flammyId];
    }

    /**
     * @dev Get the associated flammyName for a unique tokenId.
     */
    function getFlammyNameOfTokenId(uint256 _tokenId) external view returns (string memory) {
        uint8 flammyId = flammyIds[_tokenId];
        return flammyNames[flammyId];
    }

    /**
     * @dev Mint NFTs. Only the owner can call it.
     */
    function mint(
        address _to,
        string calldata _tokenURI,
        uint8 _flammyId
    ) external onlyOwner returns (uint256) {
        uint256 newId = _tokenIds.current();
        _tokenIds.increment();
        flammyIds[newId] = _flammyId;
        flammyCount[_flammyId] = flammyCount[_flammyId].add(1);
        _mint(_to, newId);
        _setTokenURI(newId, _tokenURI);
        return newId;
    }

    /**
     * @dev Set a unique name for each flammyId. It is supposed to be called once.
     */
    function setFlammyName(uint8 _flammyId, string calldata _name) external onlyOwner {
        flammyNames[_flammyId] = _name;
    }

    /**
     * @dev Burn a NFT token. Callable by owner only.
     */
    function burn(uint256 _tokenId) external onlyOwner {
        uint8 flammyIdBurnt = flammyIds[_tokenId];
        flammyCount[flammyIdBurnt] = flammyCount[flammyIdBurnt].sub(1);
        flammyBurnCount[flammyIdBurnt] = flammyBurnCount[flammyIdBurnt].add(1);
        _burn(_tokenId);
    }
}
