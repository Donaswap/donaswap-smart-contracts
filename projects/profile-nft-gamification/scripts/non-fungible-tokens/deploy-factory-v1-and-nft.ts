import { ethers, network } from "hardhat";
import { parseEther } from "ethers/lib/utils";
import config from "../../config";

const currentNetwork = network.name;

const main = async () => {
  console.log("Deploying to network:", currentNetwork);

  const _totalSupplyDistributed = 600;
  const _flamePerBurn = parseEther("10");
  const _baseURI = "ipfs://";
  const _ipfsHash = "";
  const _endBlockTime = "";

  const FlammyMintingFarm = await ethers.getContractFactory("FlammyMintingFarm");

  const flammyMintingFarm = await FlammyMintingFarm.deploy(
    config.FlameToken[currentNetwork],
    _totalSupplyDistributed,
    _flamePerBurn,
    _baseURI,
    _ipfsHash,
    _endBlockTime
  );

  await flammyMintingFarm.deployed();
  console.log("FlammyMintingFarm deployed to:", flammyMintingFarm.address);

  const donaswapFlammiesAddress = await flammyMintingFarm.donaswapFlammies();
  console.log("DonaswapFlammies deployed to:", donaswapFlammiesAddress);
};

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
