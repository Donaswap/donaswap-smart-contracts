import { assert } from "chai";
import { constants, expectEvent, expectRevert } from "@openzeppelin/test-helpers";

import { artifacts, contract, ethers } from "hardhat";

const DonaswapFlammies = artifacts.require("./DonaswapFlammies.sol");

contract("DonaswapFlammies", ([alice, bob, carol]) => {
  let donaswapFlammies;
  let result;

  before(async () => {
    const _testBaseURI = "ipfs://ipfs/";
    donaswapFlammies = await DonaswapFlammies.new(_testBaseURI, { from: alice });
  });

  // Check ticker and symbols are correct
  describe("The NFT contract is properly deployed.", async () => {
    it("Symbol is correct", async () => {
      result = await donaswapFlammies.symbol();
      assert.equal(result, "PB");
    });
    it("Name is correct", async () => {
      result = await donaswapFlammies.name();
      assert.equal(result, "Donaswap Flammies");
    });
    it("Total supply is 0", async () => {
      result = await donaswapFlammies.totalSupply();
      assert.equal(result, "0");
      result = await donaswapFlammies.balanceOf(alice);
      assert.equal(result, "0");
    });
    it("Owner is Alice", async () => {
      result = await donaswapFlammies.owner();
      assert.equal(result, alice);
    });
  });

  // Verify that ERC721 tokens can be minted, deposited and transferred
  describe("ERC721 are correctly minted, deposited, transferred", async () => {
    let testTokenURI = "testURI";
    let testflammyId1 = "3";
    let testflammyId2 = "1";

    it("NFT token is minted properly", async () => {
      result = await donaswapFlammies.mint(alice, testTokenURI, testflammyId1, {
        from: alice,
      });
      expectEvent(result, "Transfer", {
        from: constants.ZERO_ADDRESS,
        to: alice,
        tokenId: "0",
      });
      result = await donaswapFlammies.totalSupply();
      assert.equal(result, "1");
      result = await donaswapFlammies.tokenURI("0");
      assert.equal(result, "ipfs://ipfs/testURI");
      result = await donaswapFlammies.balanceOf(alice);
      assert.equal(result, "1");
      result = await donaswapFlammies.ownerOf("0");
      assert.equal(result, alice);
      result = await donaswapFlammies.getFlammyId("0");
      assert.equal(result, "3");
    });

    it("NFT token is transferred to Bob", async () => {
      result = await donaswapFlammies.safeTransferFrom(alice, bob, "0", {
        from: alice,
      });
      expectEvent(result, "Transfer", {
        from: alice,
        to: bob,
        tokenId: "0",
      });
      result = await donaswapFlammies.balanceOf(alice);
      assert.equal(result, "0");
      result = await donaswapFlammies.balanceOf(bob);
      assert.equal(result, "1");
      result = await donaswapFlammies.ownerOf("0");
      assert.equal(result, bob);
    });

    it("Second token is minted to Bob", async () => {
      result = await donaswapFlammies.mint(bob, testTokenURI, testflammyId2, {
        from: alice,
      });
      expectEvent(result, "Transfer", {
        from: constants.ZERO_ADDRESS,
        to: bob,
        tokenId: "1",
      });
      result = await donaswapFlammies.totalSupply();
      assert.equal(result, "2");
      result = await donaswapFlammies.balanceOf(bob);
      assert.equal(result, "2");
      result = await donaswapFlammies.getFlammyId("1");
      assert.equal(result, "1");
      await expectRevert(
        donaswapFlammies.safeTransferFrom(alice, bob, "0", {
          from: alice,
        }),
        "ERC721: transfer caller is not owner nor approved"
      );
    });

    it("Alice let Carol spend her NFT", async () => {
      await expectRevert(
        donaswapFlammies.approve(carol, "1", { from: alice }),
        "ERC721: approve caller is not owner nor approved for all"
      );

      result = await donaswapFlammies.approve(carol, "1", { from: bob });
      expectEvent(result, "Approval", {
        owner: bob,
        approved: carol,
        tokenId: "1",
      });
    });
  });
});
